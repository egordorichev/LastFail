#ifndef MOB_HPP_
#define MOB_HPP_

#include "Object.hpp"
#include "Timer.hpp"

#include "Item/Item.hpp"
#include "Animation/Animation.hpp"
#include "Physics/RigidBody.hpp"

#include "Mob/AI/AI.hpp"
#include "Mob/MobInfo.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <string>
#include <vector>
#include <thread>

namespace LGE{

	class Player;

	class Mob : public RigidBody{
		public:
			Mob(MobInfo *info, AI *ai, sf::Texture &texture);
			~Mob();

			std::string getName() { return this->info->getName(); };

			void draw(sf::RenderWindow *window);
			void update(std::vector <Item *> items, sf::Time dt);

			AI *getAI() { return this->ai; };

			void spawn(int x, int y); // In blocks
 			void heal(int HP);
			void kill();
			void setTarget(Object *target) { this->target = target; };
			void reciveDamage(int damage);
			void reciveKnockBack(sf::Vector2f kb) { this->velocity += kb; };

			int getDefense() { return this->info->getDefense(); };
			int getAItype() { return this->info->getAItype(); };
			int getHP() { return this->HP; };
			int getMaxHP() { return this->info->getMaxHP(); };
			int getLeft() { return this->rect.left; };
			int getTop() { return this->rect.top; };
			int getWidth() { return this->rect.width; };
			int getHegiht() { return this->rect.height; };
			int getDamage() { return this->info->getDamage(); };

			float getKbResistance() { return this->info->getKbResistance(); };
			float getKb() { return this->info->getKb(); };
			float getAttackTime() { return this->info->getAttackTime(); };

			bool isActive() { return this->active; };
			virtual bool isFriendly() { return this->info->isFriendly(); };
			bool isDead() { return this->HP == 0; };

			std::thread **getThreads() { return this->threads; };
		protected:
			sf::Sprite sprite;

			std::thread *threads[1];

			Object *target;

			MobInfo *info;
			AI *ai;
			Animation *animation;

			bool active;
			int HP;
			int threadStatuses[1];
	};

};

#endif // MOB_HPP_