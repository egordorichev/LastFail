#ifndef THREAD_HPP_
#define THREAD_HPP_

#include <thread>
#include <functional>

namespace LGE{

	class Thread{
		public:
			Thread(std::function <void()> function, bool paused = false);
			~Thread();

			void run();
			void join();

			bool isRunning() { return /*(this->status == 1)*/1; }
		private:
			std::thread thread;
			std::function <void()> function;

			int status;
	};

	void addThread(std::function <void()> function);

};

#endif // THREAD_HPP_
