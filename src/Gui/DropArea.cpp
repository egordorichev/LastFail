#include "Gui/DropArea.hpp"

namespace LGE{

	namespace gui{

		DropArea::DropArea(Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font) : Element(super,
				window, texture, font) {

			this->enabled = true;
		}

		void DropArea::draw() {
			if(this->dropped != nullptr && !this->hidden) {
				this->dropped->draw();
			}
		}

		void DropArea::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);

				if(this->dropped != nullptr) {
					this->dropped->draw();
				}
			}
		}

		void DropArea::enable() {
			this->enabled = true;
		}

		void DropArea::disable() {
			this->enabled = false;
		}

		void DropArea::hold(Draggable *object) {
			this->dropped = object;

			object->setSuper(this->super);
			object->setPosition(this->position);
		}

		bool DropArea::isEmpty() {

		}

		Element *DropArea::getDropped() {
			return this->dropped;
		}

		void DropArea::recalculatePosition() {

		}

		sf::Vector2i DropArea::getTotalSize() {

		}

	};

};