#ifndef GAMESTATEGP_HPP_
#define GAMESTATEGP_HPP_

#include "Game/Game.hpp"
#include "Game/GameState.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

namespace LGE{

	class GameStateGP : public GameState{
		public:
			GameStateGP(Game *game);

			void draw(sf::Time &dt);
			void update(sf::Time &dt);
			void handleInput();
		private:

	};

};

#endif // GAMESTATEGP_HPP_
