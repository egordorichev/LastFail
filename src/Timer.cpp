#include "Timer.hpp"

namespace LGE{

	Timer::Timer(float time, std::function <void()> callback, bool paused) {
		this->time = time;
		this->callback = callback;
		this->paused = paused;
		this->currentTime = 0.0f;
	}

	void Timer::stop() {
		this->paused = true;
		this->currentTime = 0.0f;
	}

	void Timer::pause() {
		this->paused = true;
	}

	void Timer::resume() {
		this->paused = false;
	}

	void Timer::start() {
		this->paused = false;
		this->currentTime = 0.0f;
	}

	bool Timer::update(sf::Time &dt) {
		if(!this->paused) {
			this->currentTime += dt.asSeconds();

			if(this->currentTime >= this->time) {
				this->paused = true;
				this->callback();

				return true;
			}

			return false;
		}
	}

};