#ifndef LOG_HPP_
#define LOG_HPP_

#include <string>
#include <iostream>
#include <ctime>

namespace LGE{

	class Log{
		public:
			Log(std::string type = "") {
				this->type = type;
			}

			Log &operator << (std::string message) {
				time_t t = time(0);
				struct tm *currentTime = std::localtime(&t);

				std::cout << "[" << currentTime->tm_mday <<
					"." << currentTime->tm_mon + 1 <<
					"." << currentTime->tm_year + 1900 <<
					" " << currentTime->tm_hour <<
					":" << currentTime->tm_min <<
					":" << currentTime->tm_sec <<
					"] " << this->type << ": " << message << "\n";	

				return *this;
			}

			Log &operator << (const char *message) {
				return *this << std::string(message);
			}
		private:
			std::string type;
	};

};

#endif // LOG_HPP_
