#include "PerlinNoise.hpp"

#include <cstdlib>
#include <ctime>
#include <cmath>

namespace LGE{

	PerlinNoise::PerlinNoise() {
		srand(time(NULL));

		this->permutation = new int[256];

		this->gardientX = new float[256];
		this->gardientY = new float[256];
		this->gardientZ = new float[256];

		for(int i = 0; i < 256; ++i) {
			this->permutation[i] = i;

			this->gardientX[i] = (float(rand()) / (RAND_MAX/2)) - 1.0f;
			this->gardientY[i] = (float(rand()) / (RAND_MAX/2)) - 1.0f;
			 this->gardientZ[i] = (float(rand()) / (RAND_MAX/2)) - 1.0f;
		}

		for (int i=0; i<256; i++) {
			int value = rand() & 255;
			int tmp = this->permutation[i];

			this->permutation[i] = this->permutation[value];
			this->permutation[value] = tmp;
		}
	}

	PerlinNoise::~PerlinNoise() {
		delete this->permutation;
		delete this->gardientX;
		delete this->gardientY;
		delete this->gardientZ;
	}


	float PerlinNoise::generateNoise(float sampleX, float sampleY, float sampleZ) {
		int x0 = int(floorf(sampleX));
		int x1 = x0 + 1;

		int y0 = int(floorf(sampleY));
		int y1 = y0 + 1;

		int z0 = int(floorf(sampleZ));
		int z1 = z0 + 1;

		float px0 = sampleX - float(x0);
		float px1 = px0 - 1.0f;

		float py0 = sampleY - float(y0);
		float py1 = py0 - 1.0f;

		float pz0 = sampleZ - float(z0);
		float pz1 = pz0 - 1.0f;

		int gIndex = this->permutation[(x0 + this->permutation[(y0 + this->permutation[z0 & 255]) & 255]) & 255];
		float d000 = this->gardientX[gIndex] * px0 + this->gardientY[gIndex] * py0 + this->gardientZ[gIndex] * pz0;
		gIndex = this->permutation[(x1 + this->permutation[(y0 + this->permutation[z0 & 255]) & 255]) & 255];
		float d001 = this->gardientX[gIndex] * px1 + this->gardientY[gIndex] * py0 + this->gardientZ[gIndex] * pz0;
		
		gIndex = this->permutation[(x0 + this->permutation[(y1 + this->permutation[z0 & 255]) & 255]) & 255];
		float d010 = this->gardientX[gIndex] * px0 + this->gardientY[gIndex] * py1 + this->gardientZ[gIndex] * pz0;
		gIndex = this->permutation[(x1 + this->permutation[(y1 + this->permutation[z0 & 255]) & 255]) & 255];
		float d011 = this->gardientX[gIndex] * px1 + this->gardientY[gIndex] * py1 + this->gardientZ[gIndex] * pz0;
		
		gIndex = this->permutation[(x0 + this->permutation[(y0 + this->permutation[z1 & 255]) & 255]) & 255];
		float d100 = this->gardientX[gIndex] * px0 + this->gardientY[gIndex] * py0 + this->gardientZ[gIndex] * pz1;
		gIndex = this->permutation[(x1 + this->permutation[(y0 + this->permutation[z1 & 255]) & 255]) & 255];
		float d101 = this->gardientX[gIndex] * px1 + this->gardientY[gIndex] * py0 + this->gardientZ[gIndex] * pz1;

		gIndex = this->permutation[(x0 + this->permutation[(y1 + this->permutation[z1 & 255]) & 255]) & 255];
		float d110 = this->gardientX[gIndex] * px0 + this->gardientY[gIndex] * py1 + this->gardientZ[gIndex] * pz1;
		gIndex = this->permutation[(x1 + this->permutation[(y1 + this->permutation[z1 & 255]) & 255]) & 255];
		float d111 = this->gardientX[gIndex] * px1 + this->gardientY[gIndex] * py1 + this->gardientZ[gIndex] * pz1;

		float wx = ((6 * px0 - 15) * px0 + 10) * px0 * px0 * px0;
		float wy = ((6 * py0 - 15) * py0 + 10) * py0 * py0 * py0;
		float wz = ((6 * pz0 - 15) * pz0 + 10) * pz0 * pz0 * pz0;

		float xa = d000 + wx * (d001 - d000);
		float xb = d010 + wx * (d011 - d010);
		float xc = d100 + wx * (d101 - d100);
		float xd = d110 + wx * (d111 - d110);
		float ya = xa + wy * (xb - xa);
		float yb = xc + wy * (xd - xc);
		float value = ya + wz * (yb - ya);

		return value;
	}

	FractalNoise::FractalNoise() {
		perlinNoise = new PerlinNoise();

		this->octaves = 8;
		this->lacunarity = 2.0f;
		this->persistence = 0.5f;
		this->baseFrequency = 1.0f;
		this->baseAmplitude = 1.0f;
	}

	FractalNoise::FractalNoise(int octaves, float persistence, float lacunarity, float frequency, float amplitude) {
		perlinNoise = new PerlinNoise();

		this->octaves = octaves;
		this->lacunarity = lacunarity;
		this->persistence = persistence;
		this->baseFrequency = baseFrequency;
		this->baseAmplitude = baseAmplitude;
	}

	FractalNoise::~FractalNoise() {
		delete this->perlinNoise;
	}

	float FractalNoise::generateNoise(float sampleX, float sampleY, float sampleZ) {
		float sum = 0;
		float requency = this->baseFrequency;
		float amplitude = this->baseAmplitude;

		for (int i=0; i<octaves; ++i) {
			sum += perlinNoise->generateNoise(sampleX * requency, sampleY * requency, sampleZ * requency) * amplitude;

			requency *= lacunarity;
			amplitude *= persistence;
		}

		return sum;
	}

};