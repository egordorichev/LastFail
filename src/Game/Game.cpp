#include "Game/Game.hpp"
#include "Game/GameState.hpp"

#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "Item/ItemManager.hpp"

#include "Player/Player.hpp"
#include "World/World.hpp"

#include <SFML/Graphics.hpp>

#include <string>
#include <stack>
#include <iostream>
#include <cstdlib>
#include <ctime>

namespace LGE{

	Game::Game(std::string name) {
		srand(time(0));

		this->name = name;

		this->loadTextures();
		this->loadFonts();

		this->itemManager.init(this, "data/items/defaultitems.json");
		this->itemManager.load();

		this->gui = new gui::Gui(&this->window, this->textureManager.getTexture("gui"),
			this->fontManager.getFont("Pixel"));
	}

	Game::~Game() {
		this->save();

		if(this->player != nullptr) {
			delete this->player;
		}

		if(this->world != nullptr) {
			delete this->world;
		}

		if(this->gui != nullptr) {
			delete this->gui;
		}

		while(!this->states.empty()) {
			delete this->states.top();

			this->states.pop();
		}
	}

	void Game::init() {
		this->window.create(sf::VideoMode(840, 630), this->name.c_str());
		this->window.setPosition(sf::Vector2i((sf::VideoMode::getDesktopMode().width / 2) - 320, (sf::VideoMode::getDesktopMode().height / 2) - 240));
		this->window.setKeyRepeatEnabled(false);
		this->window.setVerticalSyncEnabled(true);
	}

	void Game::run() {
		sf::Clock elapsedTimer;
		sf::Time dt;

		while(window.isOpen()) {
			GameState *state = this->getCurrentState();

			if(state != nullptr) {
				state->handleInput();
				state->update(dt);
				this->window.clear(sf::Color::Black);
				state->draw(dt);
				this->window.display();

				std::this_thread::sleep_for(std::chrono::milliseconds(1 / FPS - dt.asMilliseconds()));
				dt = elapsedTimer.restart();
			}
		}
	}

	GameState *Game::getCurrentState() {
		if(this->states.empty()) {
			return nullptr;
		}

		return this->states.top();
	}

	void Game::loadTextures() {
		this->textureManager.loadTexture("gui", "assets/images/gui.png");
		this->textureManager.loadTexture("heart", "assets/images/heart.png");
		this->textureManager.loadTexture("itemSlot", "assets/images/itemSlot.png");
	}

	void Game::loadFonts() {
		this->fontManager.loadFont("AndyBold", "assets/fonts/AndyBold.ttf");
		this->fontManager.loadFont("Pixel", "assets/fonts/Pixel.ttf");
	}

	void Game::save() {
		if(this->player != nullptr) {
			this->player->save();
		}

		if(this->world != nullptr) {
			this->world->save();
		}
	}

	void Game::pause() {
		this->paused = true;
	}

	void Game::resume() {
		this->paused = false;
	}

};
