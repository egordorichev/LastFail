#include "Player/ItemSlot.hpp"
#include "Item/Item.hpp"
#include "Item/IA/IA.hpp"
#include "Gui/Gui.hpp"

#include <vector>
#include <iostream>

#include <SFML/Graphics.hpp>

namespace LGE{


	ItemSlot::ItemSlot(gui::Container *super, sf::RenderWindow *window,
			sf::Texture &texture, sf::Font &font) : gui::Element(super, window,
			texture, font) {

		this->active = false;
		this->number = -1;

		this->sprite.setTextureRect(sf::IntRect(26, 0, 48, 48));
		this->sprite.setScale(sf::Vector2f(2.0f, 2.0f));
		this->sprite.setColor(sf::Color(4, 144, 255));

		this->slot = this->super->add <gui::Container> ();
		this->slot->setSize(sf::Vector2i(48, 48));

		auto label = this->slot->add <gui::Label> ("slotNum");

		label->setOrigin(gui::ELEMENTORIGIN_TL);
		label->setCharSize(10);
		label->setPosition(sf::Vector2i(3, 3));

		label = this->slot->add <gui::Label> ("numItems");

		label->setOrigin(gui::ELEMENTORIGIN_BR);
		label->setCharSize(10);
		label->setPosition(sf::Vector2i(3, 5));

		gui::DraggableImage *itemImage = new gui::DraggableImage(this->slot, window, texture, font);

		// itemImage->disable();
		itemImage->hide();
		itemImage->setOrigin(gui::ELEMENTORIGIN_C);

		auto dropArea = this->slot->add <gui::DropArea> ("itemDropArea");

		dropArea->setSize(sf::Vector2i(48, 48));
		dropArea->hold(itemImage);
	}

	ItemSlot::~ItemSlot() {
		delete this->slot;
	}

	void ItemSlot::setPosition(sf::Vector2i position) {
		this->position = position;
		this->slot->setPosition(position);

		this->onRectChange();
	}

	void ItemSlot::hide() {
		this->hidden = true;

		this->slot->hide();
	}

	void ItemSlot::show() {
		this->hidden = false;

		this->slot->show();
	}

	void ItemSlot::activate() {
		this->sprite.setColor(sf::Color(255, 215, 4));
		this->active = true;
	}

	void ItemSlot::deactivate() {
		this->sprite.setColor(sf::Color(4, 144, 255));
		this->active = false;
	}

	void ItemSlot::addItem(Item *item) {
		gui::DraggableImage *dropped = static_cast <gui::DraggableImage *> (this->slot->get <gui::DropArea> ("itemDropArea")->getDropped());

		dropped->setTexture(*item->getTexture());
		dropped->setTextureRect(item->getIA()->getTextureRect());
		dropped->show();

		this->items.push_back(item);

		this->slot->get <gui::Label> ("numItems")->setString(std::to_string(this->getNumItems()));
	}

	void ItemSlot::removeItem() {
		this->items.erase(this->items.begin());

		this->slot->get <gui::Label> ("numItems")->setString(std::to_string(this->getNumItems()));
	}

	int ItemSlot::getStackMax() {
		if(this->items.size() > 0) {
			return this->items[0]->getStackMax();
		} else {
			return -1;
		}
	}

	Item *ItemSlot::getItem() {
		if(!this->isEmpty()) {
			return items[0];
		} else {
			return nullptr;
		}
	}

	void ItemSlot::draw() {
		if(!this->hidden) {
			this->window->draw(this->sprite);
			this->slot->draw();
		}
	}

	void ItemSlot::handleInput(sf::Event *event) {
		if(!this->hidden) {
			this->checkEvents(event);
			this->slot->handleInput(event);

			if(!this->isEmpty()) {
				if(items[0]->isPlaced()) {
					this->removeItem();
				}
			}
		}
	}

	void ItemSlot::recalculatePosition() {
		//this->slot->setPosition(sf::Vector2i(this->rect.left, this->rect.top));
		this->sprite.setPosition(this->rect.left, this->rect.top);
	}

	sf::Vector2i ItemSlot::getTotalSize() {
		if(this->size == sf::Vector2i(0, 0)) {
			sf::FloatRect spriteRect = this->sprite.getLocalBounds();

			return sf::Vector2i(spriteRect.width * this->sprite.getScale().x, spriteRect.height * this->sprite.getScale().y);
		} else {
			return sf::Vector2i(this->size.x + this->padding * 2,
				this->size.y + this->padding * 2);
		}
	}

};
