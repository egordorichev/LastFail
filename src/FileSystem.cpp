#include "FileSystem.hpp"

#include <fstream>

#include <sys/types.h>
#include <sys/stat.h>

namespace LGE{

	bool createDir(std::string dirName) {
		int error = 0;

		#if defined(_WIN32)
		  error = _mkdir(dirName.c_str());
		#else 
		  error = mkdir(dirName.c_str(), 0733);
		#endif

		if(error != 0) {
			return false;
		}

		return true;
	}

	bool createFile(std::string fileName) {
		std::ifstream file(fileName);
	
		return file.good();
	}

};