#include "Tokenizer.hpp"
#include "Crypt.hpp"

#include <cstdlib>
#include <cstring>
#include <string>
#include <unordered_map>
#include <fstream>
#include <sstream>
#include <iostream>

namespace LGE{

	Tokenizer::Tokenizer(std::string filePath, char key) {
		this->file.open(filePath, std::ios::in | std::ios::binary);
		this->key = key;

		if(this->file.bad()) {
			std::cout << "Bad file\n";
		}
	}

	Tokenizer::Tokenizer() {

	}

	Tokenizer::~Tokenizer() {
		this->file.close();
	}

	void Tokenizer::goTo(int position) {

	}

	std::string Tokenizer::nextToken() {
		if(this->file.good()) {
			std::string token;

			char c;

			this->file.get(c);
			c = crypt(c, this->key); // FIXME

			while(c != ' ' && this->file.good()) {
				token += c;

				this->file.get(c);
				c = crypt(c, this->key);
			}

			return token;
		} else {
			return "-1";
		}
	}

};