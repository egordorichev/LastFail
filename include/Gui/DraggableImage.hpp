#ifndef DRAGGABLEIMAGE_HPP_
#define DRAGGABLEIMAGE_HPP_

#include "Gui/Draggable.hpp"

namespace LGE{

	namespace gui{

		class DraggableImage : public Draggable{
			public:
				DraggableImage(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				void draw();
				void handleInput(sf::Event *event);
				void setImageTexture(sf::Texture &texture);
				void setTextureRect(sf::IntRect rect);
			private:
				void recalculatePosition();
				sf::Vector2i getTotalSize();
		};

	};

};

#endif // DRAGGABLEIMAGE_HPP_
