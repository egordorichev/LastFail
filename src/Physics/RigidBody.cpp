#include "Physics/Physics.hpp"
#include "Physics/RigidBody.hpp"

#include <iostream>
#include <cstdlib>
#include <numeric>
#include <ctime>

namespace LGE{

	RigidBody::RigidBody(sf::FloatRect rect, RigidBodyType type, bool dynamic) {
		this->rect = rect;
		this->newRect = rect;
		this->bodyType = type;
		this->onGround = false;
		this->dynamic = dynamic;

		this->acceleration = sf::Vector2f(0.0f, 0.0f);
		this->velocity = sf::Vector2f(0.0f, 0.0f);
	}

	void RigidBody::updatePhysics(sf::Time dt) {
		float elapsed = dt.asSeconds();

		if(this->dynamic) {
			this->velocity.x += this->acceleration.x * elapsed; // NOTE: Maybe I'll need to add gravity by x
			this->velocity.y += this->acceleration.y * elapsed + GRAVITY;

			if(abs(this->velocity.x) > this->maxVelocity.x) {
				this->velocity.x = ((this->velocity.x > 0) ? 1 : -1) * this->maxVelocity.x;
			}

			if(abs(this->velocity.y) > this->maxVelocity.y) {
				this->velocity.y = ((this->velocity.y > 0) ? 1 : -1) * this->maxVelocity.y;
			}

			this->newRect.left += this->velocity.x * elapsed;
			this->newRect.top += this->velocity.y * elapsed;
		} else {
			this->velocity.x += this->acceleration.x * elapsed;
			this->velocity.y += this->acceleration.y * elapsed;
			this->newRect.left += this->velocity.x * elapsed;
			this->newRect.top += this->velocity.y * elapsed;

			if(abs(this->velocity.x) > this->maxVelocity.x) {
				this->velocity.x = ((this->velocity.x > 0) ? 1 : -1) * this->maxVelocity.x;
			}

			if(abs(this->velocity.y) > this->maxVelocity.y) {
				this->velocity.y = ((this->velocity.y > 0) ? 1 : -1) * this->maxVelocity.y;
			}
		}
	}

	bool RigidBody::checkCollisionAndResolve(RigidBody *object) { // TODO: add other collision types support
		bool isColliding = this->newRect.intersects(object->rect);

		if(object->bodyType != RIGIDBODY_NONE && this->bodyType != RIGIDBODY_NONE && isColliding) {

		}

		return isColliding;
	}

	void RigidBody::setMass(int mass) {
		this->mass = mass;

		// TODO: Mass need to have an influence at acceleration
	}

};