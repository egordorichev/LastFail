#ifndef INVENTORY_HPP_
#define INVENTORY_HPP_

#include "Item/Item.hpp"
#include "Player/ItemSlot.hpp"
#include "Gui/Gui.hpp"
#include "TextureManager.hpp"

#include <SFML/Graphics.hpp>

namespace LGE{

	class Player;

	class Inventory{
		friend class Player;
		public:
			Inventory(gui::Gui *gui);
			~Inventory();

			void draw();
			void handleInput(sf::Event *event);
			void addItem(Item *item, int slot);
			void open();
			void close();

			bool isOpen() { return !this->hidden; };

			int getSlotSize(int slot) { return this->slots[slot]->getNumItems(); };
			int getItemId(int slot);
			int getActiveSlot() { return this->activeSlot; };

			Item *getItem(int slot);
			ItemSlot *operator[](int slot) { return this->slots[slot]; };
		private:
			gui::Gui *gui;
			gui::Container *hotbar;
			gui::Container *mainInventory;
			gui::Container *coins;
			gui::Container *armor;

			/* Slot
			 * Appointment:
			 * 0-9 - Hot bar
			 * 10-49 - Main inventory
			 * 50-53 - Coin slots
			 * 54-56 - Armor slots
			 */

			ItemSlot *slots[57];

			bool hidden;
			int activeSlot;
	};

};

#endif // INVENTORY_HPP_
