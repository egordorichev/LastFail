#ifndef ANIMALAI_HPP_
#define ANIMALAI_HPP_

#include "Mob/AI/AI.hpp"

namespace LGE{

	class AnimalAI : public AI{
		public:
			/**
			 * Initializates class
			 * @param game: pointer to the game
			 * @overridden
			 */

			AnimalAI(Game *game) : AI(game) { };

			/**
			 * Proccess AI
			 * @param character: pointer to Mob or NPC
			 * @overridden
			 */

			void proccess(Object *character, Object *target) {
				// TODO
			};

			/**
			 * Returns AI name
			 * @return: AI name
			 * @overridden
			 */

			std::string getName() { return "AnimalAI"; };

			/**
			 * Returns AI id
			 * @return: AI id
			 * @overridden
			 */

			int getId() { return 2; };
		private:

	};

};

#endif // ANIMALAI_HPP_