#ifndef PERLINNOISE_HPP_
#define PERLINNOISE_HPP_

namespace LGE{

	class PerlinNoise{
		public:
			PerlinNoise();
			~PerlinNoise();

			float generateNoise(float sampleX, float sampleY, float sampleZ);
		private:
			int *permutation;

			float *gardientX;
			float *gardientY;
			float *gardientZ;
	};

	class FractalNoise{
		public:
			FractalNoise();
			FractalNoise(int octaves, float persistence, float lacunarity, float frequency, float amplitude);
			~FractalNoise();

			void setOctaves(int octaves) { this->octaves = octaves; };
			void setPersistence(float persistence) { this->persistence = persistence; };
			void setLacunarity(float lacunarity) { this->lacunarity = lacunarity; };
			void setBaseFrequency(float frequency) { this->baseFrequency = frequency; };
			void setBaseAmplitude(float amplitude) { this->baseAmplitude = amplitude; };
		
			float generateNoise(float sampleX, float sampleY, float sampleZ);
		private:
			PerlinNoise *perlinNoise;

			int octaves;
			
			float persistence;
			float lacunarity;
			float baseFrequency;
			float baseAmplitude;
	};

};

#endif // PERLINNOISE_HPP_