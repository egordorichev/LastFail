#ifndef TIMER_HPP_
#define TIMER_HPP_

#include <SFML/System.hpp>

#include <functional>

namespace LGE{

	class Timer{
		public:
			Timer(float time, std::function <void()> callback = []() {},
				bool paused = false);

			void stop();
			void pause();
			void resume();
			void start();
			void setTime(float time) { this->time = time; };
			void setCallback(std::function <void()> callback) { this->callback = callback; };

			float getCurrentTime() { return this->currentTime; };
			float getTime() { return this->time; };

			bool isEnded() { return this->currentTime >= time; };
			bool update(sf::Time &dt);
			bool isPaused() { return this->paused; };
		private:
			std::function <void()> callback;

			float currentTime;
			float time;

			bool paused;
	};

};

#endif // TIMER_HPP_