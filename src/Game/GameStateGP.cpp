#include "Game/Game.hpp"
#include "Game/GameState.hpp"
#include "Game/GameStateGP.hpp"

#include "Player/Player.hpp"

#include "Gui/Gui.hpp"
#include "World/World.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <string>
#include <iostream>

namespace LGE{

	GameStateGP::GameStateGP(Game *game) {
		this->game = game;
		this->game->zoom = 1.0f;
		this->game->paused = false;

		sf::Vector2f position = sf::Vector2f(this->game->window.getSize());

		this->game->gameView.setSize(position);
		this->game->guiView.setSize(position);

		position *= 0.5f;

		this->game->gameView.setCenter(position);
		this->game->guiView.setCenter(position);

		this->game->player = new Player(this->game);
		this->game->player->load("data/players/player.plr");

		this->game->world = new World(this->game);
		this->game->world->load("PrettyWorld");

		auto fpsLabel = this->game->gui->add <gui::Label> ("fpsLabel");

		fpsLabel->setString(std::to_string(this->game->fps));
		fpsLabel->setOrigin(gui::ELEMENTORIGIN_BL);
		fpsLabel->setPosition(sf::Vector2i(20, 20));
		fpsLabel->setColor(sf::Color(46, 204, 113));

		this->game->fpsUpdateInterval = new Interval(1.0f, [&]() {
			this->game->gui->get <gui::Label> ("fpsLabel")->setString(std::to_string(this->game->fps));
		});

		this->game->gui->recalculatePositions();
	}

	void GameStateGP::update(sf::Time &dt) {
		this->game->fpsUpdateInterval->update(dt);

		if(!this->game->isPaused()) {
			int x = this->game->player->getLeft() / 16;
			int y = this->game->player->getTop() / 16;

			/*Item *neighbors[20] = {
				this->game->world->getItem(x - 1, y - 1), this->game->world->getItem(x, y - 1),
				this->game->world->getItem(x + 1, y - 1), this->game->world->getItem(x + 2, y - 1),
				this->game->world->getItem(x - 1, y), this->game->world->getItem(x, y),
				this->game->world->getItem(x + 1, y), this->game->world->getItem(x + 2, y),
				this->game->world->getItem(x - 1, y + 1), this->game->world->getItem(x, y + 1),
				this->game->world->getItem(x + 1, y + 1), this->game->world->getItem(x + 2, y + 1),
				this->game->world->getItem(x - 1, y + 2), this->game->world->getItem(x, y + 2),
				this->game->world->getItem(x + 1, y + 2), this->game->world->getItem(x + 2, y + 2),
				this->game->world->getItem(x - 1, y + 3), this->game->world->getItem(x, y + 3),
				this->game->world->getItem(x + 1, y + 3), this->game->world->getItem(x + 2, y + 3),
			};*/

			this->game->player->update(nullptr, dt);
			this->game->world->update(dt);
		}
	}

	void GameStateGP::draw(sf::Time &dt) {
		this->game->world->drawBackgrounds(&this->game->window);

		this->game->window.setView(this->game->gameView);
		this->game->gameView.setCenter((int)(this->game->player->getLeft() +
			this->game->player->getWidth() / 2),
			(int)(this->game->player->getTop() +
			this->game->player->getHeight() / 3 * 1.5));

		this->game->world->draw(&this->game->window, dt);
		this->game->player->draw(&this->game->window, dt);

		this->game->window.setView(this->game->guiView);
		this->game->gui->draw();

		float currentTime = this->game->fpsTimer.restart().asSeconds();

		this->game->fps = 1.f / currentTime;
		this->game->lastTime = currentTime;
	}

	void GameStateGP::handleInput() {
		while(this->game->window.pollEvent(this->game->event)) {
			sf::Event &event = this->game->event;

			switch(event.type) {
				case sf::Event::Closed:
					this->game->window.close();
				break;
				case sf::Event::KeyPressed:
					if(event.key.code == sf::Keyboard::Escape) {
						if(this->game->paused) {
							this->game->resume();
						} else {
							this->game->pause();
						}
					} else if(event.key.code == sf::Keyboard::F1) {

					} else if(event.key.code == sf::Keyboard::F2) {
						this->game->world->spawnMob(1, this->game->player->getLeft() / 16 + 10, this->game->player->getTop() / 16 - 5);
					} else if(event.key.code == sf::Keyboard::F3) {
						this->game->world->spawnMob(2, this->game->player->getLeft() / 16 + 10, this->game->player->getTop() / 16 - 5);
					}
				break;
				case sf::Event::Resized:
					sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);

					this->game->gameView.reset(visibleArea);
					this->game->guiView.reset(visibleArea);
					this->game->gameView.zoom(this->game->zoom);
					this->game->guiView.zoom(1.0f);
				break;
			}

			this->game->gui->handleInput(&event);
			this->game->world->handleInput(&event);

			if(!this->game->isPaused()) {
				this->game->player->handleInput(&event);
			}
		}
	}

};
