#include "Game/GameStateMenu.hpp"
#include "Game/GameStateGP.hpp"

namespace LGE{

	GameStateMenu::GameStateMenu(Game *game) {
		this->game = game;

		auto logo = this->game->gui->add <gui::Image> ("logo");

		logo->setImageTexture(this->game->textureManager.getTexture("logo", "assets/images/logo.png"));
		logo->setTextureRect(sf::IntRect(0, 0, 320, 75));
		logo->setOrigin(gui::ELEMENTORIGIN_C);
		logo->setPosition(sf::Vector2i(0, -150));

		auto container = this->game->gui->add <gui::Container> ("navigation");

		container->setOrigin(gui::ELEMENTORIGIN_C);
		container->setPosition(sf::Vector2i(0, 100));

		auto element = container->add <gui::Button> ("singlePlayer");

		element->setLabel("Single Player");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 0));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {
			this->game->gui->get <gui::Container> ("navigation")->hide();
			this->game->gui->get <gui::Image> ("logo")->hide();
			this->game->gui->get <gui::Label> ("copyright")->hide();

			this->game->pushState(new GameStateGP(this->game));
		});

		element = container->add <gui::Button> ("multiPlayer");

		element->setLabel("Multi Player");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 54));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {
			// Go multiPlayer
		});

		element = container->add <gui::Button> ("settings");

		element->setLabel("Settings");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 108));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {
			this->game->gui->get <gui::Container> ("navigation")->hide();
			this->game->gui->get <gui::Image> ("logo")->hide();
			this->game->gui->get <gui::Container> ("settings[main]")->show();
		});

		element = container->add <gui::Button> ("exit");

		element->setLabel("Exit the game");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 182));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {
			this->game->window.close();
		});

		auto label = this->game->gui->add <gui::Label> ("copyright");

		label->setString(L"Copyright Egor Dorichev © 2016");
		label->setOrigin(gui::ELEMENTORIGIN_BR);
		label->setPosition(sf::Vector2i(10, 10));

		// Settings setup

		container = this->game->gui->add <gui::Container> ("settings[main]");

		container->setOrigin(gui::ELEMENTORIGIN_C);

		label = container->add <gui::Label> ("settings[main][label]");

		label->setString(L"Settings");
		label->setCharSize(24);
		label->setPosition(sf::Vector2i(0, -54));

		element = container->add <gui::Button> ("settings[main][general]");

		element->setLabel("General settings");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 0));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {

		});

		element = container->add <gui::Button> ("settings[main][video]");

		element->setLabel("Video settings");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 54));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {

		});

		element = container->add <gui::Button> ("settings[main][controls]");

		element->setLabel("Control settings");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 108));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {

		});

		element = container->add <gui::Button> ("settings[main][back]");

		element->setLabel("Back to menu");
		element->setSize(sf::Vector2i(256, 24));
		element->setPosition(sf::Vector2i(0, 182));

		element->addEventListener("onMouseButtonUp", [&](sf::Event *event) {
			this->game->gui->get <gui::Container> ("navigation")->show();
			this->game->gui->get <gui::Image> ("logo")->show();
			this->game->gui->get <gui::Container> ("settings[main]")->hide();

			// TODO: save
		});

		container->hide();
		this->game->gui->recalculatePositions();
	}

	void GameStateMenu::draw(sf::Time &dt) {
		//this->game->window.setView(this->game->gameView);

		this->game->window.setView(this->game->guiView);
		this->game->gui->draw();
	}

	void GameStateMenu::update(sf::Time &dt) {

	}

	void GameStateMenu::handleInput() {
		while(this->game->window.pollEvent(this->game->event)) {
			sf::Event &event = this->game->event;

			switch(event.type) {
				case sf::Event::Closed:
					this->game->window.close();
				break;
				case sf::Event::Resized:
					sf::FloatRect visibleArea(0, 0, event.size.width, event.size.height);

					this->game->gameView.reset(visibleArea);
					this->game->guiView.reset(visibleArea);
					this->game->gameView.zoom(this->game->zoom);
					this->game->guiView.zoom(1.0f);
				break;
			}

			this->game->gui->handleInput(&event);
		}
	}

};