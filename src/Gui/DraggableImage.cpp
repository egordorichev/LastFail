#include "Gui/DraggableImage.hpp"

namespace LGE{

	namespace gui{

		DraggableImage::DraggableImage(Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font) : Draggable(super,
				window, texture, font) {

		}

		void DraggableImage::draw() {
			if(!this->hidden) {
				this->window->draw(sprite);
			}
		}

		void DraggableImage::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);
			}
		}

		void DraggableImage::recalculatePosition() {
			this->sprite.setPosition(this->rect.left, this->rect.top);
		}

		sf::Vector2i DraggableImage::getTotalSize() {
			if(this->size == sf::Vector2i(0, 0)) {
				sf::FloatRect spriteRect = this->sprite.getLocalBounds();

				return sf::Vector2i(spriteRect.width * this->sprite.getScale().x, spriteRect.height * this->sprite.getScale().y);
			} else {
				return sf::Vector2i(this->size.x + this->padding * 2,
					this->size.y + this->padding * 2);
			}
		}

		void DraggableImage::setImageTexture(sf::Texture &texture) {
			this->sprite.setTexture(texture);

			this->onRectChange();
		}

		void DraggableImage::setTextureRect(sf::IntRect rect) {
			this->sprite.setTextureRect(rect);

			this->onRectChange();
		}

	};

};