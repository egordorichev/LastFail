#ifndef WARNINGLOG_HPP_
#define WARNINGLOG_HPP_

#include "Log/Log.hpp"

namespace LGE{

	static Log warningLog("Warning");

};

#endif // WARNINGLOG_HPP_