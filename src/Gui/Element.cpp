#include "Gui/Element.hpp"
#include "Gui/Container.hpp"

#include "Log/InfoLog.hpp"

#include <iostream>

namespace LGE{

	namespace gui{

		Element::Element(Container *super, sf::RenderWindow *window,
			sf::Texture &texture, sf::Font &font) : texture(texture),
			font(font) {

			this->window = window;
			this->super = super;
			this->padding = 10;
			this->state = ELEMENTSTATE_NORMAL;
			this->origin = ELEMENTORIGIN_TL;

			this->sprite.setTexture(texture);
			this->sprite.setScale(2.0f, 2.0f);

			this->onRectChange();
		}

		void Element::addEventListener(std::string eventType,
				std::function <void(sf::Event *)> callback) {

			if(eventType == "onMouseButtonDown") {
				this->callbacks[0].push_back(callback);
			} else if(eventType == "onMouseButtonUp") {
				this->callbacks[1].push_back(callback);
			} else if(eventType == "onMouseIn") {
				this->callbacks[2].push_back(callback);
			} else if(eventType == "onMouseOut") {
				this->callbacks[3].push_back(callback);
			} else {
				errorLog << "Unknown eventType \"" + eventType + "\"";
			}
		}

		void Element::checkEvents(sf::Event *event) {
			sf::Vector2i mousePosition = sf::Mouse::getPosition(*this->window);

			if(this->rect.contains(mousePosition)) {
				if(event->type == sf::Event::MouseButtonPressed && this->state != ELEMENTSTATE_MOUSEDOWN) {
					this->state = ELEMENTSTATE_MOUSEDOWN;

					for(auto callback : this->callbacks[0]) {
						callback(event);
					}
				} else if(event->type == sf::Event::MouseButtonReleased && this->state != ELEMENTSTATE_MOUSEIN) {
					this->state = ELEMENTSTATE_MOUSEIN;

					for(auto callback : this->callbacks[1]) {
						callback(event);
					}
				} else if(this->state != ELEMENTSTATE_MOUSEIN) {
					this->state = ELEMENTSTATE_MOUSEIN;

					for(auto callback : this->callbacks[2]) {
						callback(event);
					}
				}
			} else {
				if(this->state != ELEMENTSTATE_NORMAL) {
					this->state = ELEMENTSTATE_NORMAL;

					for(auto callback : this->callbacks[3]) {
						callback(event);
					}
				}
			}

			if(event->type == sf::Event::Resized) {
				this->onRectChange();
			}
		}

		void Element::setSuper(Container *super) {
			this->super = super;

			this->onRectChange();
		}

		void Element::setPadding(int padding) {
			this->padding = padding;

			this->onRectChange();
		}

		void Element::setSize(sf::Vector2i size) {
			this->size = size;

			this->onRectChange();
		}

		void Element::setPosition(sf::Vector2i position) {
			this->position = position;

			this->onRectChange();
		}

		void Element::setOrigin(ElementOrigin origin) {
			this->origin = origin;

			this->onRectChange();
		}

		void Element::onRectChange() {
			sf::Vector2i superPosition;
			sf::Vector2u superSize;

			if(this->super != nullptr) {
				superPosition = sf::Vector2i(this->super->getRect().left, this->super->getRect().top);


				if(this->super->getSuper() != nullptr) {
					superSize = sf::Vector2u(this->super->getRect().width, this->super->getRect().height);
				} else {
					superSize = this->window->getSize();
				}
			}

			if(this->size == sf::Vector2i(0, 0)) {
				sf::Vector2i totalSize = this->getTotalSize();

				this->rect.width = totalSize.x;
				this->rect.height = totalSize.y;
			} else {
				this->rect.width = this->size.x + this->padding * 2;
				this->rect.height = this->size.y + this->padding * 2;
			}

			switch(this->origin) {
				case ELEMENTORIGIN_TL:
					this->rect.left = this->position.x + superPosition.x;
					this->rect.top = this->position.y + superPosition.y;
				break;
				case ELEMENTORIGIN_TR:
					this->rect.left = superSize.x - this->position.x - this->rect.width + superPosition.x;
					this->rect.top = this->position.y + superPosition.y;
				break;
				case ELEMENTORIGIN_BR:
					this->rect.left = superSize.x - this->position.x - this->rect.width + superPosition.x;
					this->rect.top = superSize.y - this->position.y - this->rect.height + superPosition.y;
				break;
				case ELEMENTORIGIN_BL:
					this->rect.left = this->position.x + superPosition.x;
					this->rect.top = superSize.y - this->position.y - this->rect.height + superPosition.y;
				break;
				case ELEMENTORIGIN_C:
					this->rect.left = (superSize.x - this->rect.width) / 2 +
						this->position.x + superPosition.x;

					this->rect.top = (superSize.y - this->rect.height) / 2 +
						this->position.y + superPosition.y;
				break;
			}

			this->recalculatePosition();
		}

	};

};
