#ifndef SWORDIA_HPP_
#define SWORDIA_HPP_

#include "Item/IA/IA.hpp"
#include "World/World.hpp"
#include "Game/Game.hpp"

namespace LGE{

	class SwordIA : public IA{
		public:
			SwordIA(Game *game) : IA(game) { };

			void onPlace(Item *item) { };

			void onDestroy(Item *item) { };

			void onRightClick(Item *item) {

			};

			void onLeftClick(Item *item) {
				item->getSprite()->setOrigin(0, 16);
				item->getSprite()->setRotation(270);
				std::this_thread::sleep_for(std::chrono::milliseconds(int(item->getUseTime() * 1000 / 3)));
				item->getSprite()->setRotation(360);
				std::this_thread::sleep_for(std::chrono::milliseconds(int(item->getUseTime() * 1000 / 3)));
				item->getSprite()->setRotation(90);
				std::this_thread::sleep_for(std::chrono::milliseconds(int(item->getUseTime() * 1000 / 3)));
				item->getSprite()->setRotation(0);

				int x = (this->game->getEvent()->mouseButton.x +
					this->game->getPlayer()->getLeft() -
					this->game->getWindow()->getSize().x / 2) + 16;

				int y = (this->game->getEvent()->mouseButton.y - 5 +
					this->game->getPlayer()->getTop() -
					this->game->getWindow()->getSize().y / 2) + 32;

				for(Mob *mob : *this->game->getWorld()->getMobs()) {
					if(mob->getRect().contains(x, y)) {
						mob->reciveDamage(item->getDamage());
						mob->reciveKnockBack(sf::Vector2f(item->getKb(),
							item->getKb()));
					}
				}
			};

			void onRectChange(Item *item) {

			};

			void proccess(Item *item) {

			};

			sf::IntRect getTextureRect() { return sf::IntRect(0, 0, 16, 16); };

			std::string getName() { return "SwordIA"; };

			int getId() { return 2; };
		private:

	};

};

#endif // SWORDIA_HPP_