#ifndef FONTMANAGER_HPP_
#define FONTMANAGER_HPP_

#include <SFML/Graphics.hpp>

#include <unordered_map>
#include <string>

namespace LGE{

	class FontManager{
		public:
			void loadFont(std::string name, std::string fileName);

			sf::Font &getFont(std::string name);
		private:
			std::unordered_map <std::string, sf::Font> fonts;
	};

};

#endif // FONTMANAGER_HPP_