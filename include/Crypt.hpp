#ifndef CRYPT_HPP_
#define CRYPT_HPP_

#include <string>
#include <ostream>

namespace LGE{

    std::string crypt(std::string string, std::string key);
    std::string crypt(std::string string, char key);

	char crypt(char c, char key);

	struct HexStruct{
		std::string data;
		HexStruct(std::string data) : data(data) { }
	};

	std::ostream &operator << (std::ostream &ostream, const HexStruct &hexStruct);
	HexStruct hex(std::string data);


};

#endif // CRYPT_HPP_