#ifndef AI_HPP_
#define AI_HPP_

#include "Object.hpp"
#include "Player/Player.hpp"
#include "Game/Game.hpp"

#include <SFML/System.hpp>

#include <string>

namespace LGE{

	/**
	 * Artificial intelligence
	 */

	class AI{
		public:
			/**
			 * Initializates class
			 * @param game: pointer to the game
			 */

			AI(Game *game) { this->game = game; };

			/**
			 * Proccess AI
			 * @param character: pointer to Mob or NPC
			 * @param target: will follow this target
			 */

			virtual void proccess(Object *character, Object *target) { };

			/**
			 * Returns AI name
			 * @return: AI name
			 */

			virtual std::string getName() { return "AI"; };

			/**
			 * Returns AI id
			 * @return: AI id
			 */

			virtual int getId() { return 0; };
		protected:
			Game *game;
	};

};

#endif // AI_HPP_