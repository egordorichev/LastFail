#include "Json.hpp"
#include "Crypt.hpp"

#include <jsoncpp/json.h>

#include <fstream>
#include <string>
#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <algorithm>

namespace LGE{

	Json::Value loadJSON(std::string filePath) {
		std::string jsonCode;
        std::stringstream stream;

		std::fstream file(filePath, std::ios::in | std::ios::binary);

		Json::Value root;
		Json::Reader reader;

        stream << file.rdbuf();
		file.close();

    	jsonCode = stream.str();
        jsonCode.erase(std::remove(jsonCode.begin(), jsonCode.end(), '\0'), jsonCode.end());

		reader.parse(jsonCode.c_str(), root);

		return root;
	}

	Json::Value loadEncryptedJSON(std::string filePath, std::string key) {
		std::string jsonCode;
        std::stringstream stream;

		std::fstream file(filePath, std::ios::in | std::ios::binary);

		Json::Value root;
		Json::Reader reader;

		stream << file.rdbuf();
		jsonCode = crypt(stream.str(), key);

		file.close();

		/*std::fstream eraser(filePath, std::ios::trunc | std::ios::out | std::ios::binary);
		eraser.close();

		file.open(filePath, std::ios::out | std::ios::binary);
		file << (jsonCode);

		file.close();*/

		jsonCode.erase(std::remove(jsonCode.begin(), jsonCode.end(), '\0'), jsonCode.end());
		reader.parse(jsonCode.c_str(), root);

		return root;
	}

};