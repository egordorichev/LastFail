#ifndef PLAYER_HPP_
#define PLAYER_HPP_

#include "Object.hpp"

#include "Player/Inventory.hpp"
#include "Player/PlayerState.hpp"
#include "Game/Game.hpp"

#include "Physics/Physics.hpp"
#include "Physics/RigidBody.hpp"

#include "TextureManager.hpp"

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>

#include <string>
#include <stack>

namespace LGE{

	enum Flip{
		LEFT = 1,
		RIGHT = 2
	};

	class Player : public RigidBody{
		friend class PlayerState;
		friend class PlayerStateStanding;
		friend class PlayerStateRunning;
		friend class PlayerStateJumping;
		friend class PlayerStateBreaking;
		friend class PlayerStateFalling;
		friend class PlayerStateFlying;
		friend class PlayerStateDead;

		public:
			Player(Game *game);
			~Player();

			void handleInput(sf::Event *event);
			void update(Item *neighbors[20], const sf::Time &dt);
			void draw(sf::RenderWindow *window, const sf::Time &dt);
			void save() { this->save(this->file); };
			void save(std::string file);
			void load(std::string file);

			void setMaxHP(int max);
			void setHP(int HP);
			void heal(int HP);
			void setMaxMana(int max);
			void setMana(int mana);
			void regenMana(int mana); // TODO: find better name
			void setSpeed(float speed) { this->speed = speed; };
			void damage(int damage);
			void updateHPBar();
			void reciveKnockBack(sf::Vector2f knockBack);
			void pushState(PlayerState *state);
			void popState();

			int getHP() { return this->HP; };
			int getMaxHP() { return this->maxHP; };
			int getMana() { return this->mana; };
			int getMaxMana() { return this->maxMana; };
			int getDefense() { return this->defense; };

			float getSpeed() { return this->speed; }; // In pixel's per second

			bool isDead() { return this->HP == 0; };

			static PlayerStateStanding StateStanding;
			static PlayerStateRunning StateRunning;
			static PlayerStateJumping StateJumping;
			static PlayerStateBreaking StateBreaking;
			static PlayerStateFalling StateFalling;
			static PlayerStateFlying StateFlying;
			static PlayerStateDead StateDead;
		private:
			std::string file;
			std::string name;
			std::stack <PlayerState *> states;

			char key = '\"'; // Super secret!

			sf::Vector2i spawnpoint;

			Game *game;
			Inventory *inventory;

			sf::Sprite sprite;
			Flip flip;

			int HP;
			int maxHP;
			int mana;
			int maxMana;
			int defense;

			float speed;

			void setFlip(Flip flip);
	};

};

#endif // PLAYER_HPP_
