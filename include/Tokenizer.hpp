#ifndef TOKENIZER_HPP_
#define TOKENIZER_HPP_

#include "Crypt.hpp"

#include <cstdlib>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

namespace LGE{

	class Tokenizer{
		public:
			Tokenizer(std::string filePath, char key);
			Tokenizer();
			~Tokenizer();

			void goTo(int position);

			std::string nextToken();
		private:
			std::ifstream file;

			char key;
	};

};

#endif // TOKENIZER_HPP_