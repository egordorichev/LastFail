#ifndef BUTTON_HPP_
#define BUTTON_HPP_

#include "Gui/Element.hpp"

namespace LGE{

	namespace gui{

		class Button : public Element{
			public:
				Button(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				void draw();
				void handleInput(sf::Event *event);
				void setLabel(std::string label);
			private:
				sf::Text label;
				
				void recalculatePosition();
				sf::Vector2i getTotalSize();
		};

	};

};

#endif // BUTTON_HPP_