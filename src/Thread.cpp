#include "Thread.hpp"

#include <vector>

namespace LGE{

	namespace priv{

		std::vector <std::thread> workers;

	};

	Thread::Thread(std::function <void()> function, bool paused) : function(function), thread() {
		this->status = 0;

		if(!paused) {
			this->run();
		}
	}

	Thread::~Thread() {
		this->join();
	}

	void Thread::run() {
		if(!this->isRunning()) {
			this->status = 1;
			this->thread.detach();

			this->thread = std::thread([&]() {
				this->function();
				this->status = 0;
			});
		}
	}

	void Thread::join() {
		this->thread.join();
		this->thread.detach();
	}

	void addThread(std::function <void()> function) {
		priv::workers.push_back(std::thread(function));
	}

};
