#ifndef WORLD_HPP_
#define WORLD_HPP_

#include "Player/Player.hpp"

#include "World/Time.hpp"
#include "World/Chunk.hpp"

#include "Item/Item.hpp"
#include "Item/ItemManager.hpp"

#include "Mob/Mob.hpp"
#include "Mob/MobManager.hpp"

#include "TextureManager.hpp"
#include "Fader.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <string>
#include <vector>

#define NOT_GENERATED -1
#define WORLDS_DIRECTORY "data/worlds/"
#define ACTIVE_BLOCKS 32

namespace LGE{

	class World{
		public:
			World(Game *game);
			~World();

			void load(std::string name);
			void save();
			void generate(std::string name);
			void spawnMob(int id, int x, int y);
			void placeItem(Item *item, int x, int y);
			void draw(sf::RenderWindow *window, sf::Time &dt);
			void drawBackgrounds(sf::RenderWindow *window);
			void update(sf::Time &dt);
			void handleInput(sf::Event *event);
			void preloadChunkInThread(int x, int y);

			int getWidth() { return this->width; };
			int getHeight() { return this->height; };

			bool isNight() { return (this->time->getHours() >= 10 && this->time->getHours() < 7); };
			bool isMorning() { return (this->time->getHours() >= 7 && this->time->getHours() < 11); };
			bool isDay() { return (this->time->getHours() >= 11 && this->time->getHours() < 6); };
			bool isEvening() { return (this->time->getHours() >= 6 && this->time->getHours() < 10); };

			std::string getName() { return this->name; };
			std::vector <Mob *> *getMobs() { return &this->mobs; };

			Chunk *getChunk(int x, int y);
			Chunk *preloadChunk(int x, int y);
			Time *getTime() { return this->time; };
		 	Item *getItem(int x, int y);
		private:
			std::string name;
			std::string directory;
			std::vector <Mob *> mobs;
			std::vector <Chunk *> loadedChunks;

			sf::Sprite sky;

			Chunk *chunks[512][64];

			Game *game;
			MobManager *mobManager;
			Time *time;

			int width; // In blocks
			int height; // In blocks

			bool loaded;

			void preloadVisiableChunks();
			void updateChunkNeighbors(Chunk *chunk);
	};

};

#endif // WORLD_HPP_
