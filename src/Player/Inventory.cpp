#include "Player/Inventory.hpp"
#include "Player/ItemSlot.hpp"

#include "Item/Item.hpp"

#include <SFML/Window.hpp>
#include <iostream>

namespace LGE{


	Inventory::Inventory(gui::Gui *gui) {
		this->gui = gui;
		this->hidden = true;
		this->activeSlot = 0;

		this->hotbar = this->gui->add <gui::Container> ("inventory[hotbar]");
		this->hotbar->setPosition(sf::Vector2i(20, 20));

		for(int i = 0; i < 10; i++) {
			this->slots[i] = this->hotbar->add <ItemSlot> ("inventory[" + std::to_string(i) + "]");

			this->slots[i]->setNumber((i + 1) % 10); // % 10 is used to replace 10 with 0
			this->slots[i]->setPosition(sf::Vector2i(51 * (i % 10), 51 * (i / 10)));
		}

		this->mainInventory = this->gui->add <gui::Container> ("inventory[mainInventory]");
		this->mainInventory->setPosition(sf::Vector2i(20, 23));

		for(int i = 10; i < 50; i++) {
			this->slots[i] = this->mainInventory->add <ItemSlot> ("inventory[" + std::to_string(i) + "]");

			this->slots[i]->setPosition(sf::Vector2i(51 * (i % 10), 51 * (i / 10)));
			this->slots[i]->hide();
		}

		this->coins = this->gui->add <gui::Container> ("inventory[coins]");
		this->coins->setPosition(sf::Vector2i(530, 74));

		for(int i = 50; i < 54; i++) {
			this->slots[i] = this->coins->add <ItemSlot> ("inventory[" + std::to_string(i) + "]");

			this->slots[i]->setPosition(sf::Vector2i(0, (i - 50) * 51));
			this->slots[i]->hide();
		}

		this->armor = this->gui->add <gui::Container> ("inventory[armor]");

		this->armor->setOrigin(gui::ELEMENTORIGIN_BR);
		this->armor->setPosition(sf::Vector2i(-30, -30));

		for(int i = 54; i < 57; i++) {
			this->slots[i] = this->armor->add <ItemSlot> ("inventory[" + std::to_string(i) + "]");

			this->slots[i]->setPosition(sf::Vector2i(0, (i - 54) * 51));
			this->slots[i]->hide();
		}

		this->slots[this->activeSlot]->activate();
	}

	Inventory::~Inventory() {
		for(int i = 0; i < 57; i++) {
			delete this->slots[i];
		}
	}

	void Inventory::draw() {

	}

	void Inventory::handleInput(sf::Event *event) {
		switch(event->type) {
			case sf::Event::MouseWheelMoved:
				this->slots[this->activeSlot]->deactivate();
				this->activeSlot += event->mouseWheel.delta;

				if(this->activeSlot > 9) {
					this->activeSlot = 0;
				} else if(activeSlot < 0) {
					this->activeSlot = 9;
				}

				this->slots[this->activeSlot]->activate();
			break;
			case sf::Event::KeyPressed:
				switch(event->key.code) {
					case sf::Keyboard::E:
						if(this->isOpen()) {
							this->close();
						} else {
							this->open();
						}
					break;
					case sf::Keyboard::Num0: case sf::Keyboard::Numpad0: // TODO: find better way to decode 0-9 keys
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 9;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num1: case sf::Keyboard::Numpad1: // FIXME: numpad key doesn't work
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 0;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num2: case sf::Keyboard::Numpad2:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 1;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num3: case sf::Keyboard::Numpad3:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 2;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num4: case sf::Keyboard::Numpad4:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 3;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num5: case sf::Keyboard::Numpad5:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 4;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num6: case sf::Keyboard::Numpad6:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 5;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num7: case sf::Keyboard::Numpad7:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 6;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num8: case sf::Keyboard::Numpad8:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 7;
						this->slots[this->activeSlot]->activate();
					break;
					case sf::Keyboard::Num9: case sf::Keyboard::Numpad9:
						this->slots[this->activeSlot]->deactivate();
						this->activeSlot = 8;
						this->slots[this->activeSlot]->activate();
					break;
				}
			break;
		}

		Item *currentItem = this->slots[this->activeSlot]->getItem();

		if(currentItem != nullptr) {
			currentItem->handleInput(event);
		}
	}

	void Inventory::addItem(Item *item, int slot) {
		if(this->slots[slot]->isEmpty()) {
			this->slots[slot]->addItem(item);
		} else {
			if(item->getId() == this->slots[slot]->getItem()->getId()) {
				this->slots[slot]->addItem(item);
			} else {
				std::cout << "Can't add another item type to my type\n";
			}
		}
	}

	Item *Inventory::getItem(int slot) {
		return this->slots[slot]->getItem();
	}

	void Inventory::open() {
		this->hidden = false;

		for(int i = 10; i < 57; i++) {
			this->slots[i]->show();
		}
	}

	void Inventory::close() {
		this->hidden = true;

		for(int i = 10; i < 57; i++) {
			this->slots[i]->hide();
		}
	}

};
