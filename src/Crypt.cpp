#include "Crypt.hpp"

#include <string>

namespace LGE{

    std::string crypt(std::string string, std::string key) {
        for(int i = 0; i < string.size(); i++) {
    		string[i] ^= key[i % key.size()];
    	}

        return string;
    }

    std::string crypt(std::string string, char key) {
        for(int i = 0; i < string.size(); i++) {
    		string[i] ^= key;
    	}

        return string;
    }

	char crypt(char c, char key) {
		return c ^= key;
	}

};