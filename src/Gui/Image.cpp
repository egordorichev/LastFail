#include "Gui/Image.hpp"

namespace LGE{

	namespace gui{

		Image::Image(Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font) : Element(super,
				window, texture, font) {

		}

		void Image::draw() {
			if(!this->hidden) {
				this->window->draw(this->sprite);
			}
		}

		void Image::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);
			}
		}

		void Image::setImageTexture(sf::Texture &texture) {
			this->sprite.setTexture(texture);

			this->onRectChange();
		}

		void Image::setTextureRect(sf::IntRect rect) {
			this->sprite.setTextureRect(rect);

			this->onRectChange();
		}

		void Image::recalculatePosition() {
			this->sprite.setPosition(this->rect.left, this->rect.top);
			/*this->sprite.setScale(this->rect.width /
				this->sprite.getLocalBounds().width, this->rect.height /
				this->sprite.getLocalBounds().height);*/
		}

		sf::Vector2i Image::getTotalSize() {
			if(this->size == sf::Vector2i(0, 0)) {
				sf::FloatRect spriteRect = this->sprite.getLocalBounds();

				return sf::Vector2i(spriteRect.width * this->sprite.getScale().x, spriteRect.height * this->sprite.getScale().y);
			} else {
				return sf::Vector2i(this->size.x + this->padding * 2,
					this->size.y + this->padding * 2);
			}
		}

	};

};