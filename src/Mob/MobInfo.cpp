#include "Mob/MobInfo.hpp"

namespace LGE{

	MobInfo::MobInfo(std::string name, std::string texturePath,
			int AIType, int id, int damage, int maxHP, int defense,
			int width, int height, float attackTime, float kbResistance,
			float kb, float spawnInterval, float spawnChance, bool friendly,
			std::vector <AnimationFrame> animationFrames) {

		this->name = name;
		this->texturePath = texturePath;
		this->AIType = AIType;
		this->id = id;
		this->damage = damage;
		this->maxHP = maxHP;
		this->defense = defense;
		this->width = width;
		this->height = height;
		this->attackTime = attackTime;
		this->kbResistance = kbResistance;
		this->kb = kb;
		this->spawnInterval = spawnInterval;
		this->spawnChance = spawnChance;
		this->friendly = friendly;
		this->animationFrames = animationFrames;
	}

};