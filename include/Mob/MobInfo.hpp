#ifndef MOBINFO_HPP_
#define MOBINFO_HPP_

#include "Animation/AnimationFrame.hpp"

#include <string>
#include <vector>

namespace LGE{

	class MobInfo{
		public:
			MobInfo(std::string name, std::string texturePath,
				int AIType, int id, int damage, int maxHP, int defense,
				int width, int height, float attackTime, float kbResistance,
				float kb, float spawnInterval, float spawnChance, bool friendly,
				std::vector <AnimationFrame> animationFrames);

			std::string getName() { return this->name; };
			std::string getTexturePath() { return this->texturePath; };

			int getAItype() { return this->AIType; };
			int getMaxHP() { return this->maxHP; };
			int getId() { return this->id; };
			int getDefense() { return this->defense; };
			int getDamage() { return this->damage; };
			int getWidth() { return this->width; };
			int getHeight() { return this->height; };

			float getAttackTime() { return this->attackTime; };
			float getKbResistance() { return this->kbResistance; };
			float getKb() { return this->kb; };
			float getSpawnInterval() { return this->spawnInterval; }
			float getSpawnChanse() { return this->spawnChance; }

			bool isFriendly() { return this->friendly; };

			std::vector <AnimationFrame> getAnimationFrames() { return this->animationFrames; };
		private:
			std::string name;
			std::string texturePath;
			std::vector <AnimationFrame> animationFrames;

			int AIType;
			int maxHP;
			int id;
			int damage;
			int defense;
			int width;
			int height;

			float attackTime;
			float kb;
			float kbResistance;
			float spawnInterval;
			float spawnChance;

			bool friendly;
	};

};

#endif MOBINFO_HPP_