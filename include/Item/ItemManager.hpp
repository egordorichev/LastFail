#ifndef ITEMMANAGER_HPP
#define ITEMMANAGER_HPP

#include "Item/Item.hpp"
#include "Item/IA/IA.hpp"

#include "TextureManager.hpp"

#include <string>
#include <unordered_map>
#include <fstream>

#include <jsoncpp/json.h>

namespace LGE{

	class Game;

    class ItemManager{
        public:
            ItemManager(Game *game, std::string databasePath);
            ItemManager();

            void load();
            void init(Game *game, std::string databasePath);
			void addItem(Json::Value *info, int id);

			IA *getIA(int IAId);
            Item *getItem(int id);
			Json::Value *getItemInfo(int id) { return &this->itemInfos[id]; };
        private:
            Game *game;

            Json::Value root;

            std::string databasePath;
            std::fstream database;

            std::unordered_map <int, Json::Value> itemInfos;
			std::unordered_map <int, IA *> IAs;

            std::string key = "Zg4tK^eQC6wY&qzL"; // Super secret!
    };

};

#endif // ITEMMANAGER_HPP
