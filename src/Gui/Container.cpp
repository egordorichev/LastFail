#include "Gui/Container.hpp"
#include "Gui/Draggable.hpp"
#include "Gui/DropArea.hpp"
#include "Gui/Element.hpp"

#include "Log/InfoLog.hpp"

namespace LGE{

	namespace gui{

		Container::Container(Container *super, sf::RenderWindow *window,
			sf::Texture &texture, sf::Font &font) : Element(super,
			window, texture, font) {

			this->padding = 0;
		}

		void Container::recalculatePositions() {
			this->onRectChange();
		}

		void Container::draw() {
			if(!this->hidden) {
				for(auto pair : this->elements) {
					pair.second->draw();
				}
			}
		}

		void Container::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);

				for(auto pair : this->elements) {
					pair.second->handleInput(event);
				}
			}
		}

		bool Container::handleDragEnd(Draggable *draggable) {
			bool solved = false;

			for(auto pair : this->elements) {
				auto element = pair.second;

				DropArea *dropArea;

				if(dropArea = dynamic_cast <DropArea *> (element)) {
					if(dropArea->getRect().intersects(draggable->getRect())) {
						sf::IntRect draggableRect = draggable->getRect();
						sf::IntRect dropAreaRect = dropArea->getRect();

						sf::Vector2i collision(draggableRect.left + draggableRect.width -
							dropAreaRect.left, draggableRect.top + draggableRect.height -
							dropAreaRect.top);

						if(dropAreaRect.width / collision.x > 0.5f &&
								dropAreaRect.height / collision.y > 0.5f) {

							solved = true;
							dropArea->hold(draggable);
						}
					}
				}
			}

			if(!solved) {
				this->super->handleDragEnd(draggable);
			}

			return solved;
		}

		void Container::removeAll() {
			for(auto pair : this->elements) {
				delete pair.second;
			}

			this->elements.clear();
		}

		void Container::remove(std::string name) {
			if(this->elements.find(name) == this->elements.end()) {
				errorLog << "Element with name \"" + name + "\" is not found in root container";
			} else {
				delete this->elements[name];

				this->elements.erase(name);
			}
		}

		void Container::recalculatePosition() {
			for(auto pair : this->elements) {
				pair.second->onRectChange();
			}
		}

		sf::Vector2i Container::getTotalSize() {
			int totalHeight = this->padding;
			int totalWidth = this->padding;

			for(auto pair : this->elements) {
				int width = pair.second->getRect().width + pair.second->getRect().left - this->rect.left;
				int height = pair.second->getRect().height + pair.second->getRect().top - this->rect.top;

				if(width > totalWidth) {
					totalWidth = width;
				}

				if(height > totalHeight) {
					totalHeight = height;
				}
			}

			return sf::Vector2i(totalWidth, totalHeight);
		}

		void Container::show() {
			this->hidden = false;

			for(auto pair : this->elements) {
			   pair.second->onRectChange();
		   	}

			this->onRectChange();
		}

		void Container::hide() {
			this->hidden = true;

			for(auto pair : this->elements) {
			   pair.second->onRectChange();
		   	}

			this->onRectChange();
		}

	};

};