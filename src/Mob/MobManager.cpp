#include "Mob/MobManager.hpp"
#include "Animation/AnimationFrame.hpp"

#include "Mob/AI/AI.hpp"
#include "Mob/AI/SlimeAI.hpp"
#include "Mob/AI/AnimalAI.hpp"
#include "Mob/AI/NpcAI.hpp"

#include "Json.hpp"

#include <iostream>

namespace LGE{

	MobManager::MobManager(Game *game) {
		this->game = game;

		this->AIs[0] = new AI(this->game);
		this->AIs[1] = new SlimeAI(this->game);
		this->AIs[2] = new AnimalAI(this->game);
		this->AIs[3] = new NpcAI(this->game);
	}

	void MobManager::load(std::string filePath) {
		Json::Value root = loadJSON(filePath);

		if(!root.isObject()) {
            std::cout << filePath << " failed to parse\n";
        } else {
			if(!root["mobs"].isArray()) {
				std::cout << filePath << " doesn't contain \'mobs\' array\n";
			} else {
				for(auto mob : root["mobs"]) {
					try {
						int id = mob["id"].asInt();

						std::vector <AnimationFrame> animationFrames;

						for(auto frame : mob["animation"]) {
							animationFrames.push_back(AnimationFrame(
									sf::IntRect(
										frame["frameRect"]["x"].asInt(),
										frame["frameRect"]["y"].asInt(),
										frame["frameRect"]["width"].asInt(),
										frame["frameRect"]["height"].asInt()
									),

									sf::seconds(frame["frameTime"].asFloat())
							));
						}

						this->mobInfos[id] = new MobInfo(
							mob["name"].asString(),
							mob["texturePath"].asString(),
							mob["ai"].asInt(),
							mob["id"].asInt(),
							mob["damage"].asInt(),
							mob["maxHP"].asInt(),
							mob["defense"].asInt(),
							mob["width"].asInt(),
							mob["height"].asInt(),
							mob["attackTime"].asFloat(),
							mob["kbResistance"].asFloat(),
							mob["kb"].asFloat(),
							mob["spawnInterval"].asFloat(),
							mob["spawnChance"].asFloat(),
							mob["friendly"].asBool(),
							animationFrames
						);

						this->game->getTextureManager()->loadTexture("mob" + std::to_string(id),
							"assets/images/mobs/" + mob["texturePath"].asString());
					} catch(Json::Exception const &) {
						std::cout << "Got error, while parsing " << filePath << "\n";
					}
				}
			}
		}
	}

	void MobManager::addMob(MobInfo *info) {
		this->mobInfos[info->getId()] = info;
	}

	void MobManager::addAI(AI *ai) {
		this->AIs[ai->getId()] = ai;
	}

	Mob *MobManager::getMob(std::string name) {
		MobInfo *info;
		AI *ai;

		for(auto mobInfo : this->mobInfos) {
			if(mobInfo.second->getName() == name) {
				info = mobInfo.second;

				break;
			}
		}

		if(info == nullptr) {
			std::cout << "Could find mob with name " << name << "\n";

			return nullptr;
		}

		ai = this->AIs[info->getAItype()];

		if(ai == nullptr) {
			std::cout << "Could find AI with id " << info->getAItype() << "\n";

			return nullptr;
		}

		return new Mob(info, ai,
			this->game->getTextureManager()->getTexture("mob" + std::to_string(info->getId())));
	}

	Mob *MobManager::getMob(int id) {
		MobInfo *info = this->mobInfos[id];
		AI *ai;

		if(info == nullptr) {
			std::cout << "Could find mob with id " << id << "\n";

			return nullptr;
		}

		ai = this->AIs[info->getAItype()];

		if(ai == nullptr) {
			std::cout << "Could find AI with id " << info->getAItype() << "\n";

			return nullptr;
		}


		return new Mob(info, ai,
			this->game->getTextureManager()->getTexture("mob" + std::to_string(info->getId())));
	}

};