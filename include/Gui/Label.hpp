#ifndef LABEL_HPP_
#define LABEL_HPP_

#include "Gui/Element.hpp"

#include <string>

namespace LGE{

	namespace gui{

		class Label : public Element{
			public:
				Label(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				void draw();
				void handleInput(sf::Event *event);
				void setString(std::string string);
				void setString(const wchar_t *string);
				void setFont(sf::Font &font);
				void setCharSize(int size);
				void setStyle(sf::Uint32 style);
				void setColor(sf::Color color);
			private:
				sf::Text label;

				void recalculatePosition();
				sf::Vector2i getTotalSize();
		};

	};

};

#endif // LABEL_HPP_