#ifndef PLAYERSTATE_HPP_
#define PLAYERSTATE_HPP_

#include "Animation/Animation.hpp"
#include "Item/Item.hpp"

#include "Timer.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <string>

namespace LGE{

	class Player;

	enum PlayerStateType{
		PLAYERSTATE_NONE = 0,
		PLAYERSTATE_STANDING = 1,
		PLAYERSTATE_RUNNING = 2,
		PLAYERSTATE_JUMPING = 3,
		PLAYERSTATE_FALLING = 4,
		PLAYERSTATE_BREAKING = 5,
		PLAYERSTATE_FLYING = 6,
		PLAYERSTATE_DEAD = 7
	};

	std::string playerStateToString(PlayerStateType state);

	class PlayerState{
		public:
			PlayerState() { };

			virtual void update(sf::Time dt) = 0;
			virtual void draw(sf::RenderWindow *window) = 0;
			virtual void handleInput(sf::Event *event) = 0;
			virtual void pause() = 0;
			virtual void resume(Player *player) = 0;

			virtual PlayerStateType getType() { return PLAYERSTATE_NONE; };
		protected:
			Player *player;
			Animation *animation;
	};

	class PlayerStateStanding : public PlayerState{
		public:
			PlayerStateStanding();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_STANDING; };
	};

	class PlayerStateRunning : public PlayerState{
		public:
			PlayerStateRunning();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_RUNNING; };
	};

	class PlayerStateJumping : public PlayerState{
		public:
			PlayerStateJumping();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_JUMPING; };
	};

	class PlayerStateFalling : public PlayerState{
		public:
			PlayerStateFalling();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_FALLING; };
	};

	class PlayerStateBreaking : public PlayerState{
		public:
			PlayerStateBreaking();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_BREAKING; };
	};

	class PlayerStateFlying : public PlayerState{
		public:
			PlayerStateFlying();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_FLYING; };
	};

	class PlayerStateDead : public PlayerState{
		public:
			PlayerStateDead();

			void update(sf::Time dt);
			void draw(sf::RenderWindow *window);
			void handleInput(sf::Event *event);
			void pause();
			void resume(Player *player);

			PlayerStateType getType() { return PLAYERSTATE_DEAD; };
		private:
			Timer *deadTimer;
	};

};

#endif // PLAYERSTATE_HPP_
