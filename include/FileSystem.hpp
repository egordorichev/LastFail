#ifndef FILESYSTEM_HPP_
#define FILESYSTEM_HPP_

#include <string>

namespace LGE{

	bool createDir(std::string dirName);
	bool createFile(std::string fileName);

};

#endif // FILESYSTEM_HPP_