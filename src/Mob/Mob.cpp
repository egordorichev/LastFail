#include "Mob/Mob.hpp"
#include "Player/Player.hpp"

namespace LGE{

	Mob::Mob(MobInfo *info, AI *ai, sf::Texture &texture) : info(info),
			RigidBody(sf::FloatRect(0, 0, info->getWidth(), info->getHeight())) {

		this->info = info;
		this->HP = info->getMaxHP();
		this->ai = ai;
		this->active = false;
		this->maxVelocity = sf::Vector2f(140.0f, 10.8f * 60);
		this->sprite.setTexture(texture);
		this->sprite.setScale(sf::Vector2f(2.0f, 2.0f));

		this->threadStatuses[0] = 0;

		this->animation = new Animation(&this->sprite);

		for(AnimationFrame frame : this->info->getAnimationFrames()) {
			this->animation->addFrame(frame);
		}
	}

	Mob::~Mob() {
		delete this->animation;
	}

	void Mob::draw(sf::RenderWindow *window) {
		if(this->active) {
			window->draw(this->sprite);
		}
	}

	void Mob::update(std::vector <Item *> items, sf::Time dt) {
		if(this->HP == 0) {
			this->active = false;
		}

		if(this->active) {
			if(this->threads[0] == nullptr || this->threadStatuses[0] == 0) {
				this->threadStatuses[0] = 1;

				if(this->threads[0] == nullptr) {
					delete this->threads[0];
				}

				this->threads[0] = new std::thread([&]() {
					this->ai->proccess(this, this->target);
					this->threadStatuses[0] = 0;
				});
			}

			this->animation->update(dt);
			this->sprite.setPosition(this->rect.left, this->rect.top);

			Player *player;

			if(player = dynamic_cast <Player *>(target)) {
				if(this->rect.intersects(player->getRect())) {
					player->damage(this->getDamage());
					player->reciveKnockBack(sf::Vector2f((player->getCenter().x - this->getCenter().x) *
					this->getKb(), (player->getCenter().y - this->getCenter().y) * this->getKb()));
				}
			}

			this->updatePhysics(dt);

			for(Item *item : items) {
				this->checkCollisionAndResolve(static_cast <RigidBody *> (item));
			}
		}
	}

	void Mob::spawn(int x, int y) {
		this->rect.left = x * 16;
		this->rect.top = y * 16;

		this->sprite.setPosition(this->rect.left, this->rect.top);

		this->active = true;
	}

	void Mob::heal(int HP) {
		this->HP += HP;

		if(this->HP > this->getMaxHP()) {
			this->HP = this->getMaxHP();
		}
	}

	void Mob::reciveDamage(int damage) {
		if(this->active) {
			this->HP -= damage;

			if(this->HP < 0) {
				this->HP = 0;
			}
		}
	}

	void Mob::kill() {

	}

};