#ifndef RIGIDBODY_HPP_
#define RIGIDBODY_HPP_

#include "Physics/Physics.hpp"

#include "Object.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <cmath>
#include <iostream>

namespace LGE{

	enum RigidBodyType{
		RIGIDBODY_ELASTIC = 1,
		RIGIDBODY_DISPLACE = 2,
		RIGIDBODY_NONE = 3
	};

	class RigidBody : public Object{
		public:
			RigidBody(sf::FloatRect rect, RigidBodyType type = RIGIDBODY_DISPLACE, bool dynamic = true);

			void setDynamic(bool dynamic) { this->dynamic = dynamic; };
			void setMass(int mass);
			void setMaxVelocity(sf::Vector2f maxVelocity) { this->maxVelocity = maxVelocity; };
			void updatePhysics(sf::Time dt);
			void applyVelocity(sf::Vector2f velocity) { this->velocity += velocity; };

			bool isDynamic() { return this->dynamic; };
			bool checkCollisionAndResolve(RigidBody *object);
			bool isOnGround() { return this->onGround; };

			int getLeft() { return this->rect.left; };
			int getRight() { return this->rect.left + this->rect.width; };
			int getTop() { return this->rect.top; };
			int getBottom() { return this->rect.top + this->rect.height; };
			int getWidth() { return this->rect.width; };
			int getHeight() { return this->rect.height; };
			int getMass() { return this->mass; };
			int getDistance(RigidBody *object) { return sqrt((int)(this->rect.left - object->rect.left) ^ 2
				+ (int)(this->rect.top - object->rect.top) ^ 2); };

			float getRestitution() { return this->restitution; };

			sf::Vector2f getCenter() { return sf::Vector2f(this->rect.left + this->rect.width / 2,
				this->rect.top + this->rect.height / 2); };

			sf::Vector2f getAcceleration() { return this->acceleration; }
			sf::Vector2f getVelocity() { return this->velocity; };

			sf::FloatRect &getRect() { return this->rect; };
		protected:
			sf::Vector2f velocity;
			sf::Vector2f acceleration;
			sf::Vector2f maxVelocity;

			sf::FloatRect rect;
			sf::FloatRect newRect;

			RigidBodyType bodyType;

			bool dynamic;
			bool onGround;

			int mass = 1;
			float restitution = 0.1f;
	};

};

#endif // RIGIDBODY_HPP
