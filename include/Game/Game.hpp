#ifndef GAME_HPP_
#define GAME_HPP_

#include "Gui/Gui.hpp"
#include "Item/ItemManager.hpp"
#include "TextureManager.hpp"
#include "FontManager.hpp"
#include "Interval.hpp"

#include <SFML/Graphics.hpp>

#include <string>
#include <stack>

#define SFML_STATIC true
#define FPS 60

namespace LGE{

	class GameState;
	class GameStateGP;
	class GameStateMenu;
	class Player;
	class World;

	class Game{
		friend class GameState;
		friend class GameStateGP;
		friend class GameStateMenu;
		public:
			Game(std::string name);
			~Game();

			void save();
			void init();
			void run();
			void pushState(GameState *state) { this->states.push(state); };
			void popState() { delete this->states.top(); this->states.pop(); };
			void loadTextures();
			void loadFonts();
			void pause();
			void resume();

			bool isPaused() { return this->paused; };

			GameState *getCurrentState();

			TextureManager *getTextureManager() { return &this->textureManager; };
			ItemManager *getItemManager() { return &this->itemManager; };
			FontManager *getFontManager() { return &this->fontManager; };

			Player *getPlayer() { return this->player; };
			World *getWorld() { return this->world; };

			std::string getName() { return this->name; };

			sf::RenderWindow *getWindow() { return &this->window; };
			sf::Event *getEvent() { return &this->event; };

			gui::Gui *getGui() { return this->gui; };
		private:
			std::stack <GameState *> states;
			std::string name;

			sf::RenderWindow window;
			sf::Clock fpsTimer;
			sf::View gameView;
			sf::View guiView;
			sf::Event event;

			gui::Gui *gui;

			TextureManager textureManager;
			ItemManager itemManager;
			FontManager fontManager;
			Interval *fpsUpdateInterval;

			Player *player;
			World *world;

			bool paused;
			float zoom;

			int fps;
			int lastTime;
	};

};

#endif // GAME_HPP_
