#ifndef TEXTUREMANAGER_HPP_
#define TEXTUREMANAGER_HPP_

#include <SFML/Graphics.hpp>

#include <unordered_map>
#include <string>

namespace LGE{

	class TextureManager{
		public:
			void loadTexture(std::string name, std::string fileName);

			sf::Texture &getTexture(std::string name, std::string fileName);
			sf::Texture &getTexture(std::string name);
		private:
			std::unordered_map <std::string, sf::Texture> textures;
	};

};

#endif // TEXTUREMANAGER_HPP_