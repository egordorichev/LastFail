#ifndef JSON_HPP_
#define JSON_HPP_

#include <jsoncpp/json.h>
#include <string>

namespace LGE{

	Json::Value loadJSON(std::string filePath);
	Json::Value loadEncryptedJSON(std::string filePath, std::string key);

};

#endif // JSON_HPP_