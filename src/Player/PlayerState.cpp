#include "Animation/Animation.hpp"

#include "Player/Player.hpp"
#include "Player/PlayerState.hpp"

#include "Item/Item.hpp"

#include <SFML/Graphics.hpp>
#include <iostream>

namespace LGE{

	PlayerStateStanding Player::StateStanding; // Hack
	PlayerStateRunning Player::StateRunning;
	PlayerStateJumping Player::StateJumping;
	PlayerStateBreaking Player::StateBreaking;
	PlayerStateFalling Player::StateFalling;
	PlayerStateFlying Player::StateFlying;
	PlayerStateDead Player::StateDead;

	std::string playerStateToString(PlayerStateType state) {
		switch(state) {
			case PLAYERSTATE_STANDING: return "PlayerStateStanding";
			case PLAYERSTATE_RUNNING: return "PlayerStateRunning";
			case PLAYERSTATE_JUMPING: return "PlayerStateJumping";
			case PLAYERSTATE_FALLING: return "PlayerStateFalling";
			case PLAYERSTATE_BREAKING: return "PlayerStateBreaking";
			case PLAYERSTATE_FLYING: return "PlayerStateFlying";
			case PLAYERSTATE_DEAD: return "PlayerStateDead";
			case PLAYERSTATE_NONE: default: return "PlayerStateNone";
		}
	}

	PlayerStateStanding::PlayerStateStanding() {
		this->animation = new Animation(nullptr);

		this->animation->addFrame(sf::IntRect(0, 0, 16, 24), sf::milliseconds(250));
		this->animation->addFrame(sf::IntRect(0, 24, 16, 24), sf::milliseconds(250));
	}

	void PlayerStateStanding::update(sf::Time dt) {
		this->animation->update(dt);

		if(this->player->velocity.y > 0.0f) {
			this->player->pushState(&Player::StateFalling);
		}
	}

	void PlayerStateStanding::draw(sf::RenderWindow *window) {
		window->draw(this->player->sprite);
	}

	void PlayerStateStanding::handleInput(sf::Event *event) {
		if(event->type == sf::Event::KeyPressed) {
			switch(event->key.code) {
				case sf::Keyboard::A: case sf::Keyboard::Left:
					this->player->setFlip(LEFT);
					this->player->pushState(&Player::StateRunning);
				break;
				case sf::Keyboard::D: case sf::Keyboard::Right:
					this->player->setFlip(RIGHT);
					this->player->pushState(&Player::StateRunning);
				break;
				case sf::Keyboard::W: case sf::Keyboard::Up:
					this->player->pushState(&Player::StateJumping);
				break;
			}
		} else if(event->type == sf::Event::KeyReleased) {
			// Do nothing
		}
	}

	void PlayerStateStanding::pause() {

	}

	void PlayerStateStanding::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
		this->player->acceleration = sf::Vector2f(0, 0);
		this->player->velocity = sf::Vector2f(0, 0);
	}

	PlayerStateRunning::PlayerStateRunning() {
		this->animation = new Animation(nullptr);

		this->animation->addFrame(sf::IntRect(0, 48, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 72, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 96, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 120, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 144, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 168, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 192, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 216, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 240, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 264, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 288, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 312, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 336, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 360, 16, 24), sf::milliseconds(40));
	}

	void PlayerStateRunning::update(sf::Time dt) {
		this->animation->update(dt);

		if(this->player->velocity.y > 0.0f) {
			this->player->pushState(&Player::StateFalling);
		}
	}

	void PlayerStateRunning::draw(sf::RenderWindow *window) {
		window->draw(this->player->sprite);
	}

	void PlayerStateRunning::handleInput(sf::Event *event) {
		if(event->type == sf::Event::KeyPressed) {
			switch(event->key.code) {
				case sf::Keyboard::W: case sf::Keyboard::Up:
					this->player->pushState(&Player::StateJumping);
				break;
				case sf::Keyboard::A: case sf::Keyboard::Left:
					if(this->player->flip != LEFT) {
						this->player->setFlip(LEFT);
						this->player->popState();
						this->player->pushState(&Player::StateRunning);
					}
				break;
				case sf::Keyboard::D: case sf::Keyboard::Right:
					if(this->player->flip != RIGHT) {
						this->player->setFlip(RIGHT);
						this->player->popState();
						this->player->pushState(&Player::StateRunning);
					}
				break;
			}
		} else if(event->type == sf::Event::KeyReleased) {
			switch(event->key.code) {
				case sf::Keyboard::A: case sf::Keyboard::Left:
				case sf::Keyboard::D: case sf::Keyboard::Right:
					this->player->popState();
				break;
			}
		}
	}

	void PlayerStateRunning::pause() {
		this->player->acceleration = sf::Vector2f(0, 0);
		this->player->velocity = sf::Vector2f(0, 0);
	}

	void PlayerStateRunning::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
		this->player->acceleration.x += ((this->player->flip == LEFT) ? -1 : 1) * this->player->speed;
	}

	PlayerStateJumping::PlayerStateJumping() {
		this->animation = new Animation(nullptr);

		this->animation->addFrame(sf::IntRect(0, 384, 16, 24), sf::milliseconds(1));
	}

	void PlayerStateJumping::update(sf::Time dt) {
		this->animation->update(dt);

		if(this->player->velocity.y > 0.0f) {
			this->player->popState();
			this->player->pushState(&Player::StateFalling);
		}
	}

	void PlayerStateJumping::draw(sf::RenderWindow *window) {
		window->draw(this->player->sprite);
	}

	void PlayerStateJumping::handleInput(sf::Event *event) {
		if(event->type == sf::Event::KeyPressed) {
			switch(event->key.code) {
				case sf::Keyboard::A: case sf::Keyboard::Left:
					this->player->setFlip(LEFT);

					Player::StateRunning.resume(this->player); // Only adds velocity :)
				break;
				case sf::Keyboard::D: case sf::Keyboard::Right:
					this->player->setFlip(RIGHT);

					Player::StateRunning.resume(this->player); // Only adds velocity :)
				break;
			}
		} else if(event->type == sf::Event::KeyReleased) {
			switch(event->key.code) {
				case sf::Keyboard::A: case sf::Keyboard::Left:
				case sf::Keyboard::D: case sf::Keyboard::Right:
					Player::StateRunning.pause();
				break;
			}
		}
	}

	void PlayerStateJumping::pause() {
		// TODO
	}

	void PlayerStateJumping::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
		this->player->velocity.y -= 350.0f; // Juuuuuuuuuuump!
	}

	PlayerStateFalling::PlayerStateFalling() {
		this->animation = new Animation(nullptr);

		this->animation->addFrame(sf::IntRect(0, 408, 16, 24), sf::milliseconds(1));
	}

	void PlayerStateFalling::update(sf::Time dt) {
		this->animation->update(dt);

		if(this->player->isOnGround()) {
			this->player->popState();
		}
	}

	void PlayerStateFalling::draw(sf::RenderWindow *window) {
		window->draw(this->player->sprite);
	}

	void PlayerStateFalling::handleInput(sf::Event *event) {
		if(event->type == sf::Event::KeyPressed) {

		} else if(event->type == sf::Event::KeyReleased) {
			switch(event->key.code) {
				case sf::Keyboard::A: case sf::Keyboard::Left:
				case sf::Keyboard::D: case sf::Keyboard::Right:
					Player::StateRunning.pause();
				break;
			}
		}
	}

	void PlayerStateFalling::pause() {
		this->player->velocity.x = 0.0f;
		this->player->acceleration.x = 0.0f;
		this->player->velocity.y = 0.0f;
		this->player->acceleration.y = 0.0f;
	}

	void PlayerStateFalling::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
	}

	PlayerStateBreaking::PlayerStateBreaking() {
		this->animation = new Animation(nullptr);

		this->animation->addFrame(sf::IntRect(0, 432, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 456, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 480, 16, 24), sf::milliseconds(40));
		this->animation->addFrame(sf::IntRect(0, 504, 16, 24), sf::milliseconds(40));
	}

	void PlayerStateBreaking::update(sf::Time dt) {
		this->animation->update(dt);
	}

	void PlayerStateBreaking::draw(sf::RenderWindow *window) {
		window->draw(this->player->sprite);
	}

	void PlayerStateBreaking::handleInput(sf::Event *event) {

	}

	void PlayerStateBreaking::pause() {

	}

	void PlayerStateBreaking::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
	}

	PlayerStateFlying::PlayerStateFlying() {
		this->animation = new Animation(nullptr, true);

		this->animation->addFrame(sf::IntRect(0, 168, 16, 24), sf::milliseconds(1000));
	}

	void PlayerStateFlying::update(sf::Time dt) {
		this->animation->update(dt);
	}

	void PlayerStateFlying::draw(sf::RenderWindow *window) {
		window->draw(this->player->sprite);
	}

	void PlayerStateFlying::handleInput(sf::Event *event) {

	}

	void PlayerStateFlying::pause() {

	}

	void PlayerStateFlying::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
	}

	PlayerStateDead::PlayerStateDead() {
		this->animation = new Animation(nullptr, true);

		this->animation->addFrame(sf::IntRect(0, 168, 16, 24), sf::milliseconds(1000));

		this->deadTimer = new Timer(3.0f);
		this->deadTimer->stop();

		// this->player->game->getGui()->labels["playerDeadLabel"]->show();
	}

	void PlayerStateDead::update(sf::Time dt) {
		if(this->deadTimer->update(dt)) {
			this->player->states.pop();

			this->player->rect.left = this->player->spawnpoint.x;
			this->player->rect.top = this->player->spawnpoint.y;

			//this->player->game->getGui()->labels["playerDeadLabel"]->hide();

			this->player->heal(this->player->getMaxHP());
			this->player->acceleration = sf::Vector2f(0.0f, 0.0f);
			this->player->velocity = sf::Vector2f(0.0f, 0.0f);

			// TODO: respawn

			this->player->updateHPBar();

			if(this->player->states.top()->getType() != PLAYERSTATE_STANDING) {
				this->player->pushState(&Player::StateStanding);
			}
		}

		this->animation->update(dt);
	}

	void PlayerStateDead::draw(sf::RenderWindow *window) {
		// window->draw(this->player->sprite);
	}

	void PlayerStateDead::handleInput(sf::Event *event) {

	}

	void PlayerStateDead::pause() {
		this->deadTimer->stop();
	}

	void PlayerStateDead::resume(Player *player) {
		this->player = player;
		this->animation->setSprite(&player->sprite);
		this->deadTimer->start();
	}

};
