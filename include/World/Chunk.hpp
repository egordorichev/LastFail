#ifndef CHUNK_HPP_
#define CHUNK_HPP_

#include "Item/Item.hpp"
#include "Item/ItemManager.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <fstream>

#define CHUNK_SIZE 64
#define WATER_LEVEL 512 // In blocks
#define ITEM_ENCRYPTION_KEY '@'

namespace LGE{

	class Chunk{
		public:
			Chunk(int x, int y);
			~Chunk();

			void draw(sf::RenderWindow *window);
			void save(std::string directory);
			void load(std::string directory, ItemManager *itemManager);
			void generate(std::string directory, ItemManager *itemManager);
			void placeItem(Item *item, int x, int y);
			void updateSpriteRects();

			Item *destroyItem(Item *item, int x, int y);

			int getX() { return this->x; }; // In chunks
			int getY() { return this->y; };

			bool isLoaded() { return this->loaded; };

			Item **&getTerrain(); // this->terrain
			Item *getItem(int x, int y); // this->terraint[x - this->x * CHUNK_SIZE ][y - this->y * CHUNK_SIZE]
		private:
			Item *terrain[CHUNK_SIZE][CHUNK_SIZE];

			int x; // In chunks
			int y;

			bool loaded;
	};

};

#endif // CHUNK_HPP_
