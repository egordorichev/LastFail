#ifndef BLOCKIA_HPP_
#define BLOCKIA_HPP_

#include "Item/IA/IA.hpp"
#include "World/World.hpp"
#include "Game/Game.hpp"

namespace LGE{

	const sf::IntRect mainBlocksTextureRects[16][3] = {
	   { sf::IntRect(81, 27, 8, 8), sf::IntRect(90, 27, 8, 8), sf::IntRect(99, 27, 8, 8) },
	   { sf::IntRect(54, 27, 8, 8), sf::IntRect(63, 27, 8, 8), sf::IntRect(72, 27, 8, 8) },
	   { sf::IntRect(81, 0, 8, 8), sf::IntRect(81, 9, 8, 8), sf::IntRect(81, 18, 8, 8) },
	   { sf::IntRect(0, 36, 8, 8), sf::IntRect(18, 36, 8, 8), sf::IntRect(36, 36, 8, 8) },
	   { sf::IntRect(54, 0, 8, 8), sf::IntRect(63, 0, 8, 8), sf::IntRect(72, 0, 8, 8) },
	   { sf::IntRect(45, 0, 8, 8), sf::IntRect(45, 9, 8, 8), sf::IntRect(45, 18, 8, 8) },
	   { sf::IntRect(0, 27, 8, 8), sf::IntRect(18, 27, 8, 8), sf::IntRect(36, 27, 8, 8) },
	   { sf::IntRect(0, 0, 8, 8), sf::IntRect(0, 9, 8, 8), sf::IntRect(0, 18, 8, 8) },
	   { sf::IntRect(108, 0, 8, 8), sf::IntRect(108, 9, 8, 8), sf::IntRect(108, 18, 8, 8) },
	   { sf::IntRect(9, 36, 8, 8), sf::IntRect(27, 36, 8, 8), sf::IntRect(45, 36, 8, 8) },
	   { sf::IntRect(54, 36, 8, 8), sf::IntRect(63, 36, 8, 8), sf::IntRect(72, 36, 8, 8) },
	   { sf::IntRect(9, 18, 8, 8), sf::IntRect(18, 18, 8, 8), sf::IntRect(27, 18, 8, 8) },
	   { sf::IntRect(9, 27, 8, 8), sf::IntRect(27, 27, 8, 8), sf::IntRect(45, 27, 8, 8) },
	   { sf::IntRect(36, 0, 8, 8), sf::IntRect(36, 9, 8, 8), sf::IntRect(36, 18, 8, 8) },
	   { sf::IntRect(9, 0, 8, 8), sf::IntRect(18, 0, 8, 8), sf::IntRect(27, 0, 8, 8) },
	   { sf::IntRect(9, 9, 8, 8), sf::IntRect(18, 9, 8, 8), sf::IntRect(27, 9, 8, 8) }
   };

   const sf::IntRect otherBlocksTextureRects[5][3] = { // For blocks with all side solid neighbors
	   { sf::IntRect(54, 9, 8, 8), sf::IntRect(63, 9, 8, 8), sf::IntRect(72, 9, 8, 8) },
	   { sf::IntRect(54, 18, 8, 8), sf::IntRect(63, 18, 8, 8), sf::IntRect(72, 18, 8, 8) },
	   { sf::IntRect(90, 0, 8, 8), sf::IntRect(90, 9, 8, 8), sf::IntRect(90, 18, 8, 8) },
	   { sf::IntRect(99, 0, 8, 8), sf::IntRect(99, 9, 8, 8), sf::IntRect(99, 18, 8, 8) },
	   { sf::IntRect(9, 9, 8, 8), sf::IntRect(18, 9, 8, 8), sf::IntRect(27, 9, 8, 8) }
   };

	int binaryToInt(int symbols[4]) {
		int result = 0;

		if(symbols[3] == 1) {
			result += 1;
		}

		if(symbols[2] == 1) {
			result += 2;
		}

		if(symbols[1] == 1) {
			result += 4;
		}

		if(symbols[0] == 1) {
			result += 8;
		}

		return result;
	}

	class BlockIA : public IA{
		public:
			BlockIA(Game *game) : IA(game) { };

			void onPlace(Item *item) {
				// this->onRectChange(item);
			};

			void onDestroy(Item *item) {

			};

			void onRightClick(Item *item) {

			};

			void onLeftClick(Item *item) {
				int x = (this->game->getEvent()->mouseButton.x +
					this->game->getPlayer()->getLeft() -
					this->game->getWindow()->getSize().x / 2) / 16 + 1;

				int y = (this->game->getEvent()->mouseButton.y - 5 /* :-( */ +
					this->game->getPlayer()->getTop() -
					this->game->getWindow()->getSize().y / 2) / 16 + 2;

				if((this->game->getWorld()->getItem(x, y - 1)->isSolid() ||
						this->game->getWorld()->getItem(x - 1, y)->isSolid() ||
						this->game->getWorld()->getItem(x + 1, y)->isSolid() ||
						this->game->getWorld()->getItem(x, y + 1)->isSolid()) &&
						!this->game->getPlayer()->getRect().contains(x * 16, y * 16)) {

					this->game->getWorld()->placeItem(item, x, y);
				}
			};

			void onRectChange(Item *item) {
				int x = item->getLeft() / 16;
				int y = item->getTop() / 16;

				World *world = this->game->getWorld();
/*
				int bySides[4] = {
					world->getItem(x - 1, y)->isSolid(), world->getItem(x, y + 1)->isSolid(),
					world->getItem(x + 1, y)->isSolid(), world->getItem(x, y - 1)->isSolid()
				};*/

				item->setTextureRect(otherBlocksTextureRects[15][item->getVariant()]);

				/*int result = binaryToInt(bySides);

				if(result == 15) { // All our side neighbors aren't empty,
					// so check for corner neighbors

					int byCorners[4] = {
						world->getItem(x - 1, y + 1)->isSolid(), world->getItem(x + 1, y + 1)->isSolid(),
						world->getItem(x + 1, y - 1)->isSolid(), world->getItem(x - 1, y - 1)->isSolid()
					};

					int index = 4;

					switch(binaryToInt(byCorners)) {
						case 12: index = 0; break;
						case 9: index = 3; break;
						case 6: index = 2; break;
						case 3: index = 1; break;
					}

					item->setTextureRect(otherBlocksTextureRects[index][item->getVariant()]);
				} else {
					item->setTextureRect(mainBlocksTextureRects[result][item->getVariant()]);
				}*/
			};

			void proccess(Item *item) {

			};

			sf::IntRect getTextureRect() { return sf::IntRect(81, 27, 8, 8); };

			std::string getName() { return "BlockIA"; };

			int getId() { return 1; };
		private:

	};

};

#endif // BLOCKIA_HPP_