#include "World/Chunk.hpp"
#include "Log/ErrorLog.hpp"

#include <boost/filesystem.hpp>

namespace LGE{

	double random() {
		return ((double)rand() / (RAND_MAX));
	}

	Chunk::Chunk(int x, int y) {
		this->x = x;
		this->y = y;
		this->loaded = false;
	}

	Chunk::~Chunk() {
		for(int i = 0; i < CHUNK_SIZE; i++) {
			for(int j = 0; j < CHUNK_SIZE; j++) {
				delete this->terrain[i][j];
			}
		}
	}

	void Chunk::draw(sf::RenderWindow *window) {
		if(this->loaded) {
			for(int i = 0; i < CHUNK_SIZE; i++) {
				for(int j = 0; j < CHUNK_SIZE; j++) {
					this->terrain[i][j]->draw(window);
				}
			}
		} else {
			errorLog << "Couldn't draw chunk: it's not loaded";
		}
	}

	void Chunk::save(std::string directory) {
		if(this->loaded) {
			std::fstream file(directory + "chunk_" + std::to_string(this->x) +
				"_" + std::to_string(this->y) + ".chk", std::ios::out | std::ios::binary);

			for(int i = 0; i < CHUNK_SIZE; i++) {
				for(int j = 0; j < CHUNK_SIZE; j++) {
					this->terrain[i][j]->save(&file, ITEM_ENCRYPTION_KEY);
				}

				file << "\n";
			}

			file << "\n";
			file << "\n";

			file.close();
		} else {
			errorLog << "Couldn't save chunk: it's not loaded";
		}
	}

	void Chunk::load(std::string directory, ItemManager *itemManager) {
		if(!this->loaded) {
			std::string path = directory + "chunk_" + std::to_string(this->x) +
				"_" + std::to_string(this->y);

			if(boost::filesystem::exists(path)) {
				std::fstream file(path, std::ios::binary);

				for(int i = 0; i < CHUNK_SIZE; i++) {
					for(int j = 0; j < CHUNK_SIZE; j++) {
						this->terrain[i][j] = Item::load(itemManager, &file, ITEM_ENCRYPTION_KEY);
						this->terrain[i][j]->place((i + this->x * CHUNK_SIZE), (j + this->y * CHUNK_SIZE));
					}
				}
			} else {
				this->generate(directory, itemManager);
			}

			this->loaded = true;
		} else {
		   errorLog << "Couldn't load chunk: it's already loaded";
		}
	}

	void Chunk::generate(std::string directory, ItemManager *itemManager) {
		if(!this->loaded) {
			int items[CHUNK_SIZE][CHUNK_SIZE];

			if(this->y * CHUNK_SIZE <= WATER_LEVEL && this->y * CHUNK_SIZE + 1 >= WATER_LEVEL) {
				int points[CHUNK_SIZE];
				int displace = 2;
				int power = pow(2, ceil(log(CHUNK_SIZE) / (log(2))));

				double roughness = 0.6;

				points[0] = CHUNK_SIZE / 2 + (random() * displace * 2) - displace;
				points[power] = CHUNK_SIZE / 2 + (random() * displace * 2) - displace;

				displace *= roughness;

				for(int i = 1; i < power; i *= 2) {
					for(int j = (power / i) / 2; j < power; j += power / i) {
						points[j] = ((points[j - (power / i) / 2] + points[j + (power / i) / 2]) / 2);
						points[j] += (random() * displace * 2) - displace;
					}

					displace *= roughness;
				}

				for(int i = 0; i < CHUNK_SIZE; i++) {
					for(int j = 0; j < CHUNK_SIZE; j++) {
						if(points[i] >= j) {
							items[i][j] = 1;
						} else {
							items[i][j] = 0;
						}
					}
				}

				// TODO: add caves and ores
			} else if(/*this->y * CHUNK_SIZE > WATER_LEVEL*/ 1) {
				for(int i = 0; i < CHUNK_SIZE; i++) {
					for(int j = 0; j < CHUNK_SIZE; j++) {
						items[i][j] = 1;
					}
				}
			}/* else {
				for(int i = 0; i < CHUNK_SIZE; i++) {
					for(int j = 0; j < CHUNK_SIZE; j++) {
						items[i][j] = 0;
					}
				}
			}
*/
			Item *itemCopies[] = {
				itemManager->getItem(0),
				itemManager->getItem(1)
			};

			for(int i = 0; i < CHUNK_SIZE; i++) {
				for(int j = 0; j < CHUNK_SIZE; j++) {
					this->terrain[i][j] = itemCopies[items[i][j]]->copy();

					this->terrain[i][j]->place((i + this->x * CHUNK_SIZE), (j + this->y * CHUNK_SIZE));
				}
			}

			this->loaded = true;
		} else {
		   errorLog << "Couldn't generate chunk: it's already loaded";
		}
	}

	void Chunk::placeItem(Item *item, int x, int y) {
		int relativeX = x - this->x * CHUNK_SIZE;
		int relativeY = y - this->y * CHUNK_SIZE;

		if(this->terrain[relativeX][relativeY]->getId() == 0) {
			delete this->terrain[relativeX][relativeY];

			item->place(x, y);
			this->terrain[relativeX][relativeY] = item;

			for(int i = relativeX - 1; i < relativeX + 2; i++) {
				for(int j = relativeY - 1; j < relativeY + 2; j++) {
					this->terrain[i][j]->updateSpriteRect();
				}
			}
		}
	}

	Item *Chunk::destroyItem(Item *item, int x, int y) {

	}

	Item *Chunk::getItem(int x, int y) {
		if(this->loaded) {
			return this->terrain[x - this->x * CHUNK_SIZE][y - this->y * CHUNK_SIZE];
		} else {
			errorLog << "Chunk is not loaded";

			return nullptr;
		}
	}

	void Chunk::updateSpriteRects() {
		if(this->loaded) {
			for(int i = 0; i < CHUNK_SIZE; i++) {
				for(int j = 0; j < CHUNK_SIZE; j++) {
					this->terrain[i][j]->updateSpriteRect();
				}
			}
		}
	}

};