#ifndef DROPAREA_HPP_
#define DROPAREA_HPP_

#include "Gui/Element.hpp"
#include "Gui/Draggable.hpp"

namespace LGE{

	namespace gui{

		class DropArea : public Element{
			public:
				DropArea(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				void draw();
				void handleInput(sf::Event *event);
				void enable();
				void disable();
				void hold(Draggable *object);

				bool isEmpty();
				bool isEnabled() { return this->enabled; };

				Element *getDropped();
			private:
				Draggable *dropped;

				bool enabled;

				void recalculatePosition();
				sf::Vector2i getTotalSize();
		};

	};

};

#endif // DROPAREA_HPP_