#include "Gui/Label.hpp"
#include "Log/InfoLog.hpp"

namespace LGE{

	namespace gui{

		Label::Label(Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font) : Element(super,
				window, texture, font) {

			this->label.setFont(font);
			this->setCharSize(12);
		}

		void Label::draw() {
			if(!this->hidden) {
				this->window->draw(this->label);
			}
		}

		void Label::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);
			}
		}

		void Label::setString(std::string string) {
			this->label.setString(string);

			this->onRectChange();
		}

		void Label::setString(const wchar_t *string) {
			this->label.setString(string);

			this->onRectChange();
		}

		void Label::setFont(sf::Font &font) {
			this->label.setFont(font);

			this->onRectChange();
		}

		void Label::setCharSize(int size) {
			this->label.setCharacterSize(size);

			this->onRectChange();
		}

		void Label::setStyle(sf::Uint32 style) {
			this->label.setStyle(style);

			this->onRectChange();
		}

		void Label::setColor(sf::Color color) {
			this->label.setColor(color);
		}

		void Label::recalculatePosition() {
			this->label.setPosition(this->rect.left, this->rect.top);
		}

		sf::Vector2i Label::getTotalSize() {
			sf::FloatRect labelRect = this->label.getLocalBounds();

			return sf::Vector2i(labelRect.width, labelRect.height);
		}

	};

};