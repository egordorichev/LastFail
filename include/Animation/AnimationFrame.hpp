#ifndef ANIMATIONFRAME_HPP_
#define ANIMATIONFRAME_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

namespace LGE{

	class AnimationFrame{
		public:
			AnimationFrame(sf::IntRect rect, sf::Time frameTime) {
				this->rect = rect;
				this->frameTime = frameTime;
			};

			AnimationFrame() {
				this->rect = sf::IntRect(0, 0, 16, 16);
				this->frameTime = sf::seconds(1.0f);
			};

			sf::Time getFrameTime() { return this->frameTime; };
			sf::IntRect getRect() { return this->rect; };
		private:
			sf::IntRect rect;
			sf::Time frameTime;
	};

};

#endif // ANIMATIONFRAME_HPP_