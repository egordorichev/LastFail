#ifndef GUI_HPP_
#define GUI_HPP_

#include <SFML/Graphics.hpp>

#include "Gui/Element.hpp"
#include "Gui/Container.hpp"
#include "Gui/Button.hpp"
#include "Gui/Image.hpp"
#include "Gui/Label.hpp"
#include "Gui/Draggable.hpp"
#include "Gui/DraggableImage.hpp"
#include "Gui/DropArea.hpp"

#include "Log/ErrorLog.hpp"

namespace LGE{

	namespace gui{

		class Container;

		class Gui{
			public:
				Gui(sf::RenderWindow *window, sf::Texture &texture, sf::Font &font) { this->root = new Container(
					nullptr, window, texture, font); };

				~Gui() { delete this->root; };

				void handleInput(sf::Event *event) { this->root->handleInput(event); };
				void draw() { this->root->draw(); };

				void loadLayout(std::string filePath) {
					// TODO
				}

				template <class T> T *add(std::string name) { return this->root->add <T> (name); };
				template <class T> T *add() { return this->root->add <T> (); };
				template <class T> T *get(std::string name) { return this->root->get <T> (name); };

				void remove(std::string name) { this->root->remove(name); };
				void removeAll() { this->root->removeAll(); };
				void recalculatePositions() { this->root->recalculatePositions(); };

				gui::Container *getRoot() { return this->root; };

				sf::RenderWindow *getWindow() { return this->root->getWindow(); };
			private:
				Container *root;
		};

	};

};

#endif // GUI_HPP_