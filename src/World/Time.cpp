#include "World/Time.hpp"

#include <SFML/System.hpp>
#include <string>

namespace LGE{


	Time::Time(int hour, int minute, int second) {
		this->setTime(hour, minute, second);
	}

	Time::Time() {
		this->setTime(0, 0, 0);
	}

	void Time::setTime(int hours, int minutes, int seconds) {
		this->hours = hours;
		this->minutes = minutes;
		this->seconds = seconds;
	}

	int Time::getTotalTime() {
		return this->getSeconds() + this->getMinutes() * 60 +
			this->getHours() * 3600;
	}

	void Time::update(sf::Time &elapsed) {
		this->seconds += elapsed.asMilliseconds() / 16.6f;

		while(this->seconds >= 60.0f) {
			this->seconds -= 60.0f;
			this->minutes += 1.0f;

			while(this->minutes >= 60.0f) {
				this->minutes -= 60.0f;
				this->hours += 1.0f;

				while(this->hours >= 24.0f) {
					this->hours -= 24.0f;
				}
			}
		}
	}

	std::string Time::asString() {
		return this->asString("h:m:s p");
	}

	std::string Time::asString(std::string format) {
		std::string time = "";

		for(char c : format) {
			switch(c) {
				case 'h':
					time += std::to_string(int(this->hours) % 12);
				break;
				case 'H':
					time += std::to_string(this->getHours());
				break;
				case 'm':
					time += std::to_string(this->getMinutes());
				break;
				case 's':
					time += std::to_string(this->getSeconds());
				break;
				case 'p':
					time += (this->hours > 12) ? "pm" : "am";
				break;
				case 'P':
					time += (this->hours > 12) ? "PM" : "AM";
				break;
				default:
					time += c;
				break;
			}
		}

		return time;
	}

};
