#ifndef ITEMSLOT_HPP_
#define ITEMSLOT_HPP_

#include "Item/Item.hpp"

#include "Gui/Gui.hpp"
#include "Gui/DropArea.hpp"

#include <SFML/Graphics.hpp>

#include <vector>

namespace LGE{

	enum ItemSlotType{
		ITEMSLOT_COIN,
		ITEMSLOT_ARMOR,
		ITEMSLOT_NORMAL
	};

	class ItemSlot : public gui::Element{
		friend class gui::Container;
		public:
			ItemSlot(gui::Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font);

			~ItemSlot();

			void hide();
			void show();
			void draw();
			void handleInput(sf::Event *event);
			void addItem(Item *item);
			void removeItem();
			void activate();
			void deactivate();
			void setPosition(sf::Vector2i position);
			void setNumber(int number) {
				this->number = number;
				this->slot->get <gui::Label>
					("slotNum")->setString((number != -1) ?
					std::to_string(number) : "");
			}


			int getNumItems() { return this->items.size(); };
			int getStackMax();

			bool isEmpty() { return this->items.empty(); };
			bool isActive() { return this->active; };

			Item *getItem();
		private:
			std::vector <Item *> items;

			gui::Container *slot;

			bool active;
			int number;

			void recalculatePosition();
			sf::Vector2i getTotalSize();
	};

};

#endif // ITEMSLOT_HPP_
