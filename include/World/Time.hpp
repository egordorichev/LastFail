#ifndef TIME_HPP_
#define TIME_HPP_

#include <SFML/System.hpp>

namespace LGE{

	class Time{
		public:
			Time(int hour, int minute, int second);
			Time();

			void setTime(int hour, int minute, int second);
			void update(sf::Time &elapsed);

			int getHours() { return int(this->hours); };
			int getMinutes() { return int(this->minutes); };
			int getSeconds() { return int(this->seconds); };
			int getTotalTime(); // In seconds

			std::string asString();
			std::string asString(std::string format);


			/* h - hour in 12-hour format
			 * H - hour in 24-hour format
			 * m - minute
			 * s - second
			 * p - am or pm in lower case
			 * P - AM or PM in upper case
			 */

			// NOTE: One second in real world equals one minute in LastFail
		private:
			float hours;
			float minutes;
			float seconds;
	};

};

#endif // TIME_HPP_
