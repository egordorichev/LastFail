#ifndef OBJECT_HPP_
#define OBJECT_HPP_

#include <thread>

namespace LGE{

	class Object{
		public:
			Object() { };

			virtual ~Object() { }; // Little hack: http://stackoverflow.com/a/15114118/4741065

			template <class T> T *isInstanceOf() { return (dynamic_cast <T *> (this)); };
		protected:

	};

};

#endif // OBJECT_HPP_
