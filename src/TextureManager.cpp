#include "TextureManager.hpp"
#include "Log/ErrorLog.hpp"

#include <SFML/Graphics.hpp>

#include <unordered_map>
#include <string>
#include <iostream>

#define IMAGES_FOLDER "assets/images/"

namespace LGE{

	void TextureManager::loadTexture(std::string name, std::string fileName) {
		sf::Texture texture;

		if(texture.loadFromFile(fileName)) {
			// texture.setSmooth(true);

			this->textures[name] = texture;
		}
	}

	sf::Texture &TextureManager::getTexture(std::string name, std::string fileName) {
		if(this->textures.find(name) == this->textures.end()) {
			this->loadTexture(name, fileName);
		}

		return this->textures[name];
	}

	sf::Texture &TextureManager::getTexture(std::string name) {
		if(this->textures.find(name) == this->textures.end()) {
			errorLog << "Couldn't find texture with name \"" + name + "\"";

			exit(0);
		} else {
			return this->textures[name];
		}
	}

};
