#include "Gui/Draggable.hpp"
#include "Gui/DropArea.hpp"
#include "Gui/Container.hpp"

namespace LGE{

	namespace gui{

		Draggable::Draggable(Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font) : Element(super,
				window, texture, font) {

			this->enabled = true;
			this->addEventListener("onMouseButtonDown", [&](sf::Event *event) {
				if(event->mouseButton.button == sf::Mouse::Left && this->enabled) {
					this->startDragging();
				}
			});

			this->addEventListener("onMouseButtonUp", [&](sf::Event *event) {
				if(event->mouseButton.button == sf::Mouse::Left) {
					this->stopDragging();
				}
			});
		}

		void Draggable::draw() {
			if(!this->hidden) {
				this->window->draw(this->sprite);
			}
		}
		void Draggable::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);

				if(this->dragging) {
					sf::Vector2i mousePosition = sf::Mouse::getPosition(*this->window);

					this->rect.left = mousePosition.x;
					this->rect.top = mousePosition.y;
				}
			}
		}

		void Draggable::returnToInitialPosition() {
			this->rect.left = this->initialPosition.x;
			this->rect.top = this->initialPosition.y;
		}

		void Draggable::startDragging() {
			std::cout << "Drag start" << std::endl;

			if(!this->dragging) {
				this->dragging = true;
				this->initialPosition = sf::Vector2i(this->rect.left, this->rect.top);
			}
		}

		void Draggable::stopDragging() {
			std::cout << "Drag end" << std::endl;

			if(this->dragging) {
				this->dragging = false;

				this->super->handleDragEnd(this);
			}
		}

		void Draggable::recalculatePosition() {

		}

		sf::Vector2i Draggable::getTotalSize() {

		}

	};

};
