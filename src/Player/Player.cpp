#include "Player/Player.hpp"
#include "Player/PlayerState.hpp"

#include "Physics/Physics.hpp"
#include "Physics/RigidBody.hpp"

#include "Game/Game.hpp"
#include "World/World.hpp"
#include "Tokenizer.hpp"
#include "Crypt.hpp"
#include "Gui/Gui.hpp"

#include "TextureManager.hpp"

#include <SFML/Graphics.hpp>

#include <string>
#include <fstream>
#include <iostream>

namespace LGE{

	Player::Player(Game *game) : RigidBody(sf::FloatRect(500 * 16, 1600, 32, 48)) {
		this->file = "";
		this->spawnpoint.x = 500 * 16;
		this->spawnpoint.y = 1600;
		this->game = game;
		this->maxHP = 400;
		this->HP = this->maxHP;
		this->mana = 20;
		this->maxMana = 20;
		this->speed = 181.76;
		this->defense = 0;

		this->inventory = new Inventory(this->game->getGui());

		// Physics setup

		this->maxVelocity = sf::Vector2f(this->speed * 1.4, 10.8f * 60);

		// Sprite setup

		this->pushState(&Player::StateStanding);

		this->sprite.setScale(sf::Vector2f(2.0f, 2.0f));
		this->sprite.setTexture(this->game->getTextureManager()->getTexture("player", "assets/images/player.png"));

		// HP setup

		gui::Container *hearts = this->game->getGui()->add <gui::Container> ("hearts");

		hearts->setOrigin(gui::ELEMENTORIGIN_TR);
		hearts->setPosition(sf::Vector2i(10, 10));

		for(int i = 0; i < this->maxHP / 20 + (this->maxHP % 20 > 0) ? 1 : 0; i++) {
			gui::Image *heart = hearts->add <gui::Image> ("heart" + std::to_string(i));

			heart->setImageTexture(this->game->getTextureManager()->getTexture("heart"));
			heart->setPosition(sf::Vector2i((i % 10) * 24, (i / 10) * 24));
		}

		gui::Label *deadLabel = this->game->getGui()->add <gui::Label>("deadLabel");

		deadLabel->setString("You were slain...");
		deadLabel->setOrigin(gui::ELEMENTORIGIN_C);
		deadLabel->hide();

		this->updateHPBar();
	}

	Player::~Player() {

	}

	void Player::handleInput(sf::Event *event) {
		PlayerState *state = this->states.top();

/*		switch(event->type) {
			case sf::Event::KeyPressed:
				switch(event->key.code) {
					case sf::Keyboard::A: case sf::Keyboard::Left:
						if(state->getType() != PLAYERSTATE_RUNNING && state->getType() != PLAYERSTATE_JUMPING && state->getType() != PLAYERSTATE_FALLING) {
							this->states.push(new PlayerStateRunning(this));
						}

						if(this->flip != LEFT) {
							this->flip = LEFT;
							this->sprite.setScale(-2.0f, 2.0f);
						}

						this->acceleration.x -= speed;
					break;
					case sf::Keyboard::D: case sf::Keyboard::Right:
						if(state->getType() != PLAYERSTATE_RUNNING && state->getType() != PLAYERSTATE_JUMPING && state->getType() != PLAYERSTATE_FALLING) {
							this->states.push(new PlayerStateRunning(this));
						}

						if(this->flip != RIGHT) {
							this->flip = RIGHT;
							this->sprite.setScale(2.0f, 2.0f);
						}

						this->acceleration.x += speed;
					break;
					case sf::Keyboard::W: case sf::Keyboard::Up:
						if(state->getType() != PLAYERSTATE_JUMPING && state->getType() != PLAYERSTATE_FALLING) {
							this->velocity.y -= 350.0f; // Juuuuuuuuuuump!

							this->states.push(new PlayerStateJumping(this));
						}
					break;
				}
			break;
			case sf::Event::KeyReleased:
				switch(event->key.code) {
					case sf::Keyboard::W: case sf::Keyboard::Up:

					break;
					case sf::Keyboard::A: case sf::Keyboard::Left:
						this->velocity.x = 0.0f;
						this->acceleration.x += speed;

						if(state->getType() == PLAYERSTATE_RUNNING) {
							this->states.pop();
						}
					break;
					case sf::Keyboard::D: case sf::Keyboard::Right:
						this->velocity.x = 0.0f;
						this->acceleration.x -= speed;

						if(state->getType() == PLAYERSTATE_RUNNING) {
							this->states.pop();
						}
					break;
					case sf::Keyboard::S: case sf::Keyboard::Down:

					break;
				}
			break;
		}*/

		this->inventory->handleInput(event);

		if(state != nullptr) {
			state->handleInput(event);
		}
	}

	void Player::update(Item *neighbors[20], const sf::Time &dt) {
		PlayerState *state = this->states.top();

		if(state->getType() != PLAYERSTATE_DEAD) { // FIXME: uncomment
			// this->updatePhysics(dt);

			/*for(int i = 0; i < 20; i++) {
				if(neighbors[i] != nullptr) {
					this->checkCollisionAndResolve(static_cast <RigidBody *>(neighbors[i]));
				}
			}*/
		}

		this->rect = this->newRect;

		/*if(this->HP == 0 && state->getType() != PLAYERSTATE_DEAD) {
			if(states.top()->getType() != PLAYERSTATE_STANDING) {
				this->states.pop();
			}

			this->states.push(new PlayerStateDead(this));
		}

		if(state->getType() != PLAYERSTATE_DEAD) {
			sf::Vector2f lastVelocity = this->velocity;

			if(state->getType() != PLAYERSTATE_DEAD) {
				this->updatePhysics(dt);

				for(int i = 19; i > -1; i--) {
					this->checkCollisionAndResolve(static_cast <RigidBody *>(neighbors[i]));
				}
			}

			if(this->velocity.y == 0.0f && state->getType() == PLAYERSTATE_FALLING) {
				this->states.pop();

				int damage = (static_cast <int> (lastVelocity.y) - 400.0f) / 5;

				if(damage > 0) {
					this->damage(damage);
				}
			}

			if(this->velocity.y > 0.0f && state->getType() != PLAYERSTATE_FALLING) {
				if(state->getType() == PLAYERSTATE_JUMPING) {
					this->states.pop();
				}

				this->states.push(new PlayerStateFalling(this));
			}
		}*/

		state->update(dt);
	}

	void Player::draw(sf::RenderWindow *window, const sf::Time &dt) {
		if(this->flip == LEFT) {
			this->sprite.setPosition((int)this->rect.left + (int)this->rect.width,
				(int)this->rect.top);
		} else {
			this->sprite.setPosition((int)this->rect.left, (int)this->rect.top);
		}

		this->states.top()->draw(window);
	}

	void Player::save(std::string file) { // FIXME: segfault
		this->file = file;
/*
		std::ofstream config(this->file, std::ios::out | std::ios::binary | std::ios::trunc);

		config << crypt(this->name + " ", this->key);
		config << crypt(std::to_string(this->spawnpoint.x) + " ", this->key);
		config << crypt(std::to_string(this->spawnpoint.y) + " ", this->key);

		for(int i = 0; i < 57; i++) {
			config << crypt(std::to_string(this->inventory->getSlotSize(i)) + " ", this->key);
			config << crypt(std::to_string(this->inventory->getItemId(i)) + " ", this->key);
		}

		config.close();*/
	}

	void Player::load(std::string file) {
		this->file = file;

		Tokenizer tokenizer(file, this->key);

		/*this->name = tokenizer.nextToken();
		this->spawnpoint = sf::Vector2i(std::stoi(tokenizer.nextToken()) * 16,
			std::stoi(tokenizer.nextToken())) * 16;

		for(int i = 0; i < 57; i++) {
			for(int j = 0; j < std::stoi(tokenizer.nextToken()); j++) {
				int id = std::stoi(tokenizer.nextToken());

				if(id != 0) {
					this->inventory->addItem(this->game->getItemManager()->getItem(id), i);
				}
			}
		}*/

		this->inventory->addItem(this->game->getItemManager()->getItem(4), 0);

		for(int i = 0; i < 999; i++) {
			this->inventory->addItem(this->game->getItemManager()->getItem(1), 1);
		}

		for(int i = 0; i < 999; i++) {
			this->inventory->addItem(this->game->getItemManager()->getItem(2), 2);
		}

		for(int i = 0; i < 999; i++) {
			this->inventory->addItem(this->game->getItemManager()->getItem(3), 3);
		}

		for(int i = 0; i < 3; i++) {
			this->inventory->addItem(this->game->getItemManager()->getItem(1), 23);
		}
	}

	void Player::pushState(PlayerState *state) {
		state->resume(this);
		this->states.push(state);
	}

	void Player::popState() {
		if(this->states.top() != nullptr) {
			this->states.top()->pause();
			this->states.pop();

			if(this->states.top() != nullptr) {
				this->states.top()->resume(this);
			}
		}
	}

	void Player::setMaxHP(int max) {
		this->maxHP = max;

		if(this->maxHP > 500) {
			this->maxHP = 500;
		}
	}

	void Player::setHP(int HP) {
		this->HP = HP;

		if(this->HP > this->maxHP) {
			this->HP = this->maxHP;
		}
	}

	void Player::heal(int HP) {
		this->HP += HP;

		if(this->HP > this->maxHP) {
			this->HP = this->maxHP;
		}
	}

	void Player::setMaxMana(int max) {
		this->maxMana = max;

		if(this->maxMana > 400) {
			this->maxMana = 400;
		}
	}

	void Player::setMana(int mana) {
		this->mana = mana;

		if(this->mana > this->maxMana) {
			this->mana = this->maxMana;
		}
	}

	void Player::regenMana(int mana) {
		this->mana += mana;

		if(this->mana > this->maxMana) {
			this->mana = this->maxMana;
		}
	}

	void Player::damage(int damage) {
		this->HP -= damage;

		if(this->HP < 0) {
			this->HP = 0;
		}

		if(this->HP > this->maxHP) {
			this->HP = this->maxHP;
		}

		this->updateHPBar();
	}

	void Player::reciveKnockBack(sf::Vector2f kb) {
		this->velocity += kb;
	}

	void Player::updateHPBar() {
		auto hearts = this->game->getGui()->get <gui::Container> ("hearts");

		for(int i = 0; i < this->maxHP / 20 + (this->maxHP % 20 > 0) ? 1 : 0; i++) {
			if(i * 20 > this->HP - 20) {
				if((this->HP - i * 20) % 20 >= 15) {
					hearts->get <gui::Image> ("heart" + std::to_string(i))->setTextureRect(sf::IntRect(11, 0, 11, 11));
				} else if((this->HP - i * 20) % 20 >= 10) {
					hearts->get <gui::Image> ("heart" + std::to_string(i))->setTextureRect(sf::IntRect(22, 0, 11, 11));
				} else if((this->HP - i * 20) % 20 >= 1) {
					hearts->get <gui::Image> ("heart" + std::to_string(i))->setTextureRect(sf::IntRect(33, 0, 11, 11));
				} else {
					hearts->get <gui::Image> ("heart" + std::to_string(i))->setTextureRect(sf::IntRect(44, 0, 11, 11));
				}
			} else {
				hearts->get <gui::Image> ("heart" + std::to_string(i))->setTextureRect(sf::IntRect(0, 0, 11, 11));
			}
		}
	}

	/*void Player::startJumping() {
		this->pushState(new PlayerStateJumping(this));
		this->velocity.y -= 350.0f;
	}

	void Player::stopJumping() {

	}

	void Player::startRunning(Flip where) {
		this->pushState(new PlayerStateRunning(this));

		if(this->flip != where) {
			this->flip = where;
			this->sprite.setScale(((this->flip == LEFT) ? -1 : 1) * 2.0f, 2.0f);
		}

		this->acceleration.x += ((this->flip == LEFT) ? -1 : 1) * this->speed;
	}

	void Player::stopRunning() {
		if(this->states.top()->getType() == PLAYERSTATE_RUNNING) {
			this->popState();
		}

		this->acceleration.x -= ((this->flip == LEFT) ? -1 : 1) * this->speed;
		this->velocity.x = 0;
		this->acceleration.x = 0;
	}

	void Player::startFalling() {

	}

	void Player::stopFalling() {

	}

	void Player::startFlying() {

	}

	void Player::stopFlying() {

	}

	void Player::startBreaking() {

	}

	void Player::stopBreaking() {

	}

	void Player::die() {
		this->HP = 0;
	}*/

	void Player::setFlip(Flip flip) {
		if(this->flip != flip) {
			this->flip = flip;
			this->sprite.setScale(((this->flip == LEFT) ? -1 : 1) * 2.0f, 2.0f);
		}
	}

};