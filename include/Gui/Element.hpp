#ifndef ELEMENT_HPP_
#define ELEMENT_HPP_

#include "TextureManager.hpp"

#include <SFML/Graphics.hpp>

#include <functional>
#include <iostream>

namespace LGE{

	namespace gui{

		enum ElementState{
			ELEMENTSTATE_MOUSEIN,
			ELEMENTSTATE_MOUSEDOWN,
			ELEMENTSTATE_NORMAL
		};

		enum ElementOrigin{
			ELEMENTORIGIN_TL,
			ELEMENTORIGIN_TR,
			ELEMENTORIGIN_BR,
			ELEMENTORIGIN_BL,
			ELEMENTORIGIN_C
		};

		class Container;

		class Element{
			friend class Container;
			public:
				Element(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				virtual void draw() = 0;
				virtual void handleInput(sf::Event *event) = 0;
				virtual void hide() { this->hidden = true; };
				virtual void show() { this->hidden = false; };

				void setTexture(sf::Texture &texture) { this->sprite.setTexture(texture); };
				void addEventListener(std::string eventType, std::function <void(sf::Event *)> callback);

				bool isHidden() { return this->hidden; };
				bool isClicked() { return this->state == ELEMENTSTATE_MOUSEDOWN; };
				bool isHovered() { return this->state == ELEMENTSTATE_MOUSEIN; };

				void setSuper(Container *super);
				void setPadding(int padding);
				void setSize(sf::Vector2i size);
				virtual void setPosition(sf::Vector2i position);
				void setOrigin(ElementOrigin origin);

				int getPadding() { return this->padding; };

				ElementState getState() { return this->state; };
				Container *getSuper() { return this->super; };

				sf::Vector2i getPosition() { return this->position; };
				sf::IntRect getRect() { return this->rect; };
				sf::RenderWindow *getWindow() { return this->window; };
			protected:
				sf::Sprite sprite;
				sf::IntRect rect;
				sf::RenderWindow *window;
				sf::Texture &texture;
				sf::Font &font;
				sf::Vector2i size;
				sf::Vector2i position;

				std::vector <std::function <void(sf::Event *)>> callbacks[4];

				Container *super;

				ElementState state;
				ElementOrigin origin;

				bool hidden;
				int padding;

				virtual void recalculatePosition() { };
				virtual sf::Vector2i getTotalSize() { return sf::Vector2i(this->padding, this->padding); };

				void onRectChange();
				void checkEvents(sf::Event *event);
		};

	};

};

#endif // ELEMENT_HPP_
