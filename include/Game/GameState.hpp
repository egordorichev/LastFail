#ifndef GAMESTATE_HPP_
#define GAMESTATE_HPP_

#include "Game/Game.hpp"

#include <SFML/System.hpp>

namespace LGE{

	class GameState{
		public:
			virtual void draw(sf::Time &dt) = 0;
			virtual void update(sf::Time &dt) = 0;
			virtual void handleInput() = 0;			
		protected:
			Game *game;
	};

};

#endif // GAMESTATE_HPP_