#include "Item/Item.hpp"
#include "Item/IA/IA.hpp"
#include "Item/ItemManager.hpp"

#include "Crypt.hpp"

#include <algorithm>
#include <iostream>

namespace LGE{

    Item::Item(Json::Value *info, IA *ia, sf::Texture &texture) : info(info),
			RigidBody(sf::FloatRect(0, 0, (*info)["width"].asInt(), (*info)["height"].asInt()),
			RIGIDBODY_DISPLACE, false) {

        this->info = info;
		this->ia = ia;
		this->variant = rand() % 3 + 0;
		this->placed = false;
		this->nameSpace = "LastFail"; // TODO

        if(this->isSolid()) {
			this->bodyType = RIGIDBODY_DISPLACE;
		} else {
			this->bodyType = RIGIDBODY_NONE;
		}

        this->setTexture(texture);
        this->sprite.setTextureRect(sf::IntRect(0, 0, 16, 16));
        this->sprite.setScale(sf::Vector2f(2.0f, 2.0f));

		this->threads[0] = new Thread([&]() {
			this->ia->onRightClick(this);
		}, true);

		this->threads[1] = new Thread([&]() {
			this->ia->onLeftClick(this);
		}, true);

		this->threads[2] = new Thread([&]() {
			this->ia->proccess(this);
		}, true);
	}

	Item::~Item() {
		for(auto thread : this->threads) {
			delete thread;
		}
	}

	void Item::handleInput(sf::Event *event) {
		if(event->type == sf::Event::MouseButtonPressed) {
			if(event->mouseButton.button == sf::Mouse::Right) {
				if(this->ia != nullptr && !this->threads[0]->isRunning()) {
					this->threads[0]->run();
				}
			} else {
				if(this->ia != nullptr && !this->threads[1]->isRunning()) {
					this->threads[1]->run();
				}
			}
		}
	}

	void Item::update(sf::Time &dt) {
		if(this->ia != nullptr && !this->threads[2]->isRunning()) {
			this->threads[2]->run();
		}
	}

	void Item::draw(sf::RenderWindow *window) {
		if(this->placed) {
			window->draw(this->sprite);
		}
	}

	void Item::place(int x, int y) {
		this->placed = true;

		this->setPosition(x * 16, y * 16);

		this->ia->onPlace(this);
	}

	void Item::destroy() {
		if(this->ia != nullptr) {
			this->ia->onDestroy(this);
			this->placed = false;
			this->updateSpriteRect();
		}
	}

	void Item::updateSpriteRect() {
		if(this->ia != nullptr) {
			this->ia->onRectChange(this);
		}
	}

	Item *Item::copy() {
		return new Item(this->info, this->ia, const_cast <sf::Texture &> (*this->sprite.getTexture())); // FIXME
	}

	Item *Item::copyFully() {
		// TODO
	}

    void Item::setPosition(int x, int y) {
        this->rect.left = x;
        this->rect.top = y;

        this->sprite.setPosition(this->rect.left, this->rect.top);
    }

	/* File is encoded with one char and look's like that:
	 * ... [2 e[0 1]] 1 [2 o[2 3]] ...
	 *      ^
	 *     id
	 */

	void Item::save(std::fstream *file, char key) {
		*file << crypt(std::to_string(this->getId()), key) << " ";
	}

	/*void Item::load(ItemManager *itemManager, std::ifstream *file, char key) {
		char symbol = crypt(file->peek(), key);

		if(symbol == '[') {
			// TODO
		} else {
			int id;

			*file >> id;

			this->info = itemManager->getItemInfo(id);

			file->get(); // Skip ' '
		}
	}*/

	Item *Item::load(ItemManager *itemManager, std::fstream *file, char key) {
		std::string id;

		*file >> id;

		return itemManager->getItem(std::stoi(crypt(id, key)));
	}

	void Item::drawAt(sf::RenderWindow *window, int x, int y) {
		sf::Vector2f position = this->sprite.getPosition(); // Todo

		this->sprite.setPosition(x, y);


		if(this->ia->getId() == 1) {
			this->sprite.setTextureRect(sf::IntRect(81, 27, 8, 8));
		}

		// Ok, fixme

		window->draw(this->sprite);

		this->sprite.setPosition(position);
	}

	int Item::getId() {
		Json::Value &param = (*this->info)["id"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 0;
	}

	int Item::getPurchaseCost() {
		Json::Value &param = (*this->info)["purchaseCost"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 0;
	}

	int Item::getSellCost() {
		Json::Value &param = (*this->info)["sellCost"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 0;
	}

	int Item::getStackMax() {
		Json::Value &param = (*this->info)["stackMax"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 0;
	}

	int Item::getDamage() {
		Json::Value &param = (*this->info)["damage"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 1; // Default damage
	}

	int Item::getWidth() {
		Json::Value &param = (*this->info)["height"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 0;
	}

	int Item::getHeight() {
		Json::Value &param = (*this->info)["height"];

		if(param.isNumeric()) {
			return param.asInt();
		}

		return 0;
	}

	float Item::getKb() {
		Json::Value &param = (*this->info)["knockBack"];

		if(param.isNumeric()) {
			return param.asFloat();
		}

		return 0.0f;
	}

	float Item::getUseTime() {
		Json::Value &param = (*this->info)["useTime"];

		if(param.isNumeric()) {
			return param.asFloat();
		}

		return 0.0f;
	}

	int Item::getVariant() {
		return this->variant;
	}


    bool Item::isPlaceable() {
		Json::Value &param = (*this->info)["placeable"];

		if(param.isBool()) {
			return param.asBool();
		}

		return false;
	}

    bool Item::isSolid() {
		Json::Value &param = (*this->info)["solid"];

		if(param.isBool()) {
			return param.asBool();
		}

		return false;
	}

	std::string Item::getName() {
		Json::Value &param = (*this->info)["description"];

		if(param.isString()) {
			return param.asString();
		}

		return 0;
	}

	std::string Item::getDescription() {
		Json::Value &param = (*this->info)["description"];

		if(param.isString()) {
			return param.asString();
		}

		return 0;
	}

};
