#ifndef NPCAI_HPP_
#define NPCAI_HPP_

#include "Mob/AI/AI.hpp"

namespace LGE{

	class NpcAI : public AI{
		public:
			/**
			 * Initializates class
			 * @param game: pointer to the game
			 * @overridden
			 */

			NpcAI(Game *game) : AI(game) { };

			/**
			 * Proccess AI
			 * @param character: pointer to Mob or NPC
			 * @overridden
			 */

			void proccess(Object *character, Object *target) {
				// TODO
			};

			/**
			 * Returns AI name
			 * @return: AI name
			 * @overridden
			 */

			std::string getName() { return "NpcAI"; };

			/**
			 * Returns AI id
			 * @return: AI id
			 * @overridden
			 */

			int getId() { return 3; };
		private:

	};

};

#endif // NPCAI_HPP_