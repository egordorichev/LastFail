#ifndef ITEM_HPP_
#define ITEM_HPP_

#include "Physics/RigidBody.hpp"

#include "Timer.hpp"
#include "Thread.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include <string>
#include <fstream>

#include <jsoncpp/json.h>

namespace LGE{

	class IA;
	class ItemManager;

    class Item : public RigidBody{
        public:
            Item(Json::Value *info, IA *ia, sf::Texture &texture);
			~Item();

			Item *drop();
			Item *copy();
			Item *copyFully();

			sf::Sprite *getSprite() { return &this->sprite; };
			sf::Texture *getTexture() { return const_cast <sf::Texture *> (this->sprite.getTexture()); };

			void update(sf::Time &dt);
			void handleInput(sf::Event *event);
			void draw(sf::RenderWindow *window);			void setTexture(sf::Texture &texture) { this->sprite.setTexture(texture); };
			void setPosition(int x, int y);
			void drawAt(sf::RenderWindow *window, int x, int y);
			void place(int x, int y);
			void destroy();
			void updateSpriteRect();
			void setTextureRect(sf::IntRect rect) { this->sprite.setTextureRect(rect); };

			void save(std::fstream *file, char key);

			static Item *load(ItemManager *itemManager, std::fstream *file, char key);

			int getId();
			int getPurchaseCost();
			int getSellCost();
			int getStackMax();
			int getDamage();
			int getWidth();
			int getHeight();
			int getVariant();

			float getKb();
			float getUseTime();

			bool isPlaceable();
			bool isSolid();
			bool isPlaced() { return this->placed; };

			std::string getName();
			std::string getDescription();
			std::string getNameSpace() { return this->nameSpace; };

			IA *getIA() { return this->ia; };
        protected:
			std::string nameSpace;

			sf::Sprite sprite;

			Json::Value *info;

			IA *ia;
			Thread *threads[3];

			bool placed;
			int variant;
    };

};

#endif // ITEM_HPP_
