#ifndef INFOLOG_HPP_
#define INFOLOG_HPP_

#include "Log/Log.hpp"

namespace LGE{

	static Log infoLog("Info");

};

#endif // INFOLOG_HPP_