
#ifndef MOBMANAGER_HPP_
#define MOBMANAGER_HPP_

#include "Mob/AI/AI.hpp"
#include "Mob/Mob.hpp"

#include "Game/Game.hpp"

#include <string>
#include <unordered_map>

namespace LGE{

	class MobManager{
		public:
			MobManager(Game *game);

			void load(std::string filePath);
			void addMob(MobInfo *info);
			void addAI(AI *ai);

			Mob *getMob(std::string name);
			Mob *getMob(int id);
		private:
			std::unordered_map <int, AI *> AIs;
			std::unordered_map <int, MobInfo *> mobInfos;

			Game *game;
	};

};

#endif // MOBMANAGER_HPP_