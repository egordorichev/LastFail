#include "FontManager.hpp"
#include "Log/ErrorLog.hpp"

#include <SFML/Graphics.hpp>

#include <unordered_map>
#include <string>

namespace LGE{

	void FontManager::loadFont(std::string name, std::string fileName) {
		sf::Font font;

		font.loadFromFile(fileName);

		this->fonts[name] = font;
	}

	sf::Font &FontManager::getFont(std::string name) {
		if(this->fonts.find(name) == this->fonts.end()) {
			errorLog << "Couldn't find fonts with name \"" + name + "\"";
		} else {
			return this->fonts[name];
		}
	}

};