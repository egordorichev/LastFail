theme = {
	name = "default",
	borderSize = 2,

	bodyColor = {
		r = 41,
		g = 128,
		b = 85
	},

	borderColor = {
		r = 44,
		g = 62,
		b = 90
	},

	textColor = {
		r = 44,
		g = 62,
		b = 90
	},

	bodyHighlightColor = {
		r = 52,
		g = 152,
		b = 219
	},

	borderHighlightColor = {
		r = 52,
		g = 73,
		b = 94
	},

	textHighlightColor = {
		r = 52,
		g = 73,
		b = 94
	}
}