#ifndef LIGHTPOINT_HPP_
#define LIGHTPOINT_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

namespace LGE{

	class LightPoint{
		public:
			LightPoint(sf::Texture &texture, sf::Vector2f position = sf::Vector2f(0, 0),
					sf::Color color = sf::Color(255, 255, 255), float radius = 128.0f) {

				this->sprite.setTexture(texture);
				this->sprite.setPosition(position);
				this->sprite.setColor(color);
				this->setRadius(radius);
			};

			void setPosition(float x, float y) { this->sprite.setPosition(x, y); };
			void setPosition(sf::Vector2f position) { this->sprite.setPosition(position); };
			void setColor(sf::Color color) { this->sprite.setColor(color); };

			void setRadius(float radius) {
				this->sprite.setScale(sf::Vector2f(radius * 2 / this->sprite.getLocalBounds().width,
					radius * 2 /this->sprite.getLocalBounds().height));
			}

			void draw(sf::RenderWindow *window) { window->draw(this->sprite); };

			sf::Texture &getTexture() { return const_cast <sf::Texture &> (*this->sprite.getTexture());	};
		private:
			sf::Sprite sprite;
	};

};

#endif // LIGHTPOINT_HPP_