#ifndef FADER_HPP_
#define FADER_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Interval.hpp"

#include <iostream>

namespace LGE{

	enum FaderFlags{
		FADER_FADEIN,
		FADER_FADEOUT
	};

	class Fader{
		public:
			Fader(float fadeTime, sf::Sprite *sprite, FaderFlags flags,
					bool paused = false) {

				this->flags = flags;
				this->sprite = sprite;

				if(this->flags == FADER_FADEOUT) {
					this->opacity = 255;
				} else {
					this->opacity = 0;
				}

				this->interval = new Interval(fadeTime / 255, [&]() {

					if(this->flags == FADER_FADEOUT) {
						this->opacity--;

						if(this->opacity == 0) {
							this->interval->stop();
						}
					} else {
						this->opacity++;

						if(this->opacity >= 255) {
							this->interval->stop();
						}
					}

					sf::Color newColor = this->sprite->getColor();
					newColor.a = this->opacity;

					this->sprite->setColor(newColor);
				}, paused);
			}

			~Fader() {
				delete this->interval;
			}

			void update(sf::Time &dt) { this->interval->update(dt); };
			void start() { this->interval->start(); };
			void pause() { this->interval->pause(); };
			void stop() { this->interval->stop(); };

			void setFadeTime(float fadeTime) { this->interval->setTime(fadeTime / 255); };

			void setFlags(FaderFlags flags) {
				if(this->flags != flags) {
					this->interval->setCallback([&]() {

						if(this->flags == FADER_FADEOUT) {
							this->opacity--;

							if(this->opacity == 0) {
								this->interval->stop();
							}
						} else {
							this->opacity++;

							if(this->opacity >= 255) {
								this->interval->stop();
							}
						}

						sf::Color newColor = this->sprite->getColor();
						newColor.a = this->opacity;

						this->sprite->setColor(newColor);
					});

					this->flags = flags;
				}
			}

			bool isPaused() { return this->interval->isPaused(); };

			bool isEnded() {
				return (this->flags == FADER_FADEOUT && this->opacity == 0) ||
					(this->flags == FADER_FADEIN && this->opacity == 255);
			}

			int getCurrentOpacity() { return this->opacity; };
		private:
			Interval *interval;
			FaderFlags flags;

			sf::Sprite *sprite;

			int opacity;
	};

};

#endif // FADER_HPP_