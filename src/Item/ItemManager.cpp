#include "Item/Item.hpp"
#include "Item/ItemManager.hpp"
#include "Item/IA/BlockIA.hpp"
#include "Item/IA/SwordIA.hpp"

#include "Log/ErrorLog.hpp"
#include "Game/Game.hpp"

#include "TextureManager.hpp"
#include "Crypt.hpp"
#include "Json.hpp"

#include <jsoncpp/json.h>

#include <cstdlib>
#include <cstring>
#include <string>
#include <unordered_map>
#include <fstream>
#include <iostream>

namespace LGE{


    ItemManager::ItemManager(Game *game, std::string databasePath) {
        this->init(game, databasePath);
    }

    ItemManager::ItemManager() {

    }

    void ItemManager::init(Game *game, std::string databasePath) {
        this->game = game;
        this->databasePath = databasePath;

		Json::Value air;

		air["id"] = 0;
		air["name"] = "Air \"Block\"";
		air["description"] = "Emptiness...";
		air["texturePath"] = "assets/images/item_0.png";
		air["IAId"] = 0;
		air["purchaseCost"] = 0;
		air["sellCost"] = 0;
		air["stackMax"] = 0;
		air["damage"] = 0;
		air["width"] = 16;
		air["height"] = 16;
		air["placeable"] = false;
		air["solid"] = false;

        this->itemInfos[0] = air;

        this->game->getTextureManager()->loadTexture("item0", "assets/images/items/item_0.png");

		this->IAs[0] = new IA(this->game);
		this->IAs[1] = new BlockIA(this->game);
		this->IAs[2] = new SwordIA(this->game);
    }

    void ItemManager::load() {
		this->root = loadJSON(this->databasePath); // TODO: replace with next line

		// this->root = loadEncryptedJSON(this->databasePath, this->key);

		if(!this->root.isObject()) {
            errorLog << this->databasePath + " failed to parse";
        } else {
			if(this->root["items"].isArray()) {
				for(auto item : this->root["items"]) {
					try {
						if(item["id"].isInt()) {
							int id = 0;

							if(item["id"].isInt()) {
								id = item["id"].asInt();
							} else {
								std::cout << "Error\n";

								return;
							}

							std::string texturePath = "";

							if(item["texturePath"].isString()) {
								texturePath = item["texturePath"].asString();
							}

							this->itemInfos[id] = item;
							this->game->getTextureManager()->loadTexture("item" +
								std::to_string(id), "assets/images/" + texturePath);
						}
					} catch(...) {
						errorLog << "Got error, while parsing " + this->databasePath;
					}
				}
			} else {
				errorLog << this->databasePath + " doesn't contain \"items\" array";
			}
        }
    }

    Item *ItemManager::getItem(int id) {
		Json::Value &info = this->itemInfos[id];

		if(info.empty()) {
			errorLog << "info->empty()";
		} else {
			Json::Value &param = info["IAId"];

			if(param.isInt()) {
				return new Item(&info, this->IAs[param.asInt()],
					this->game->getTextureManager()->getTexture("item" + std::to_string(id)));
			} else {
				errorLog << "Error";
			}
		}
    }

	IA *ItemManager::getIA(int IAId) {
		return this->IAs[IAId];
	}

};