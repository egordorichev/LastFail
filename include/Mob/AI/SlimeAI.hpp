#ifndef SLIMEAI_HPP_
#define SLIMEAI_HPP_

#include "Mob/AI/AI.hpp"
#include "Mob/Mob.hpp"

#include <iostream>

namespace LGE{

	class SlimeAI : public AI{
		public:
			/**
			 * Initializates class
			 * @param game: pointer to the game
			 * @overridden
			 */

			SlimeAI(Game *game) : AI(game) { };

			/**
			 * Proccess AI
			 * @param character: pointer to Mob or NPC
			 * @overridden
			 */

			void proccess(Object *character, Object *target) {
				if(target != nullptr) {
					Player *player;
					Mob *mob;

					if(player = dynamic_cast <Player *>(target)) {
						if(mob = dynamic_cast <Mob *>(character)) {
							float t = 1.0f;
							float xV = (player->getCenter().x - mob->getCenter().x) / t;
							float yV = -300.0f;

							if(xV > 300.f) {
								xV = 300.0f;
							} else if(xV < -300.0f) {
								xV = -300.0f;
							}

							mob->applyVelocity(sf::Vector2f(xV, yV));

							std::this_thread::sleep_for(std::chrono::milliseconds(5000));
						}
					}
				}
			};

			/**
			 * Returns AI name
			 * @return: AI name
			 * @overridden
			 */

			std::string getName() { return "SlimeAI"; };

			/**
			 * Returns AI id
			 * @return: AI id
			 * @overridden
			 */

			int getId() { return 1; };
		private:

	};

};

#endif // SLIMEAI_HPP_