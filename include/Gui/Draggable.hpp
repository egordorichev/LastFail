#ifndef DRAGGABLE_HPP_
#define DRAGGABLE_HPP_

#include "Gui/Element.hpp"

namespace LGE{

	namespace gui{

		class DropArea;

		class Draggable : public Element{
			public:
				Draggable(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				virtual void draw();
				virtual void handleInput(sf::Event *event);

				void enable() { this->enabled = true; };
				void disable() { this->enabled = false; };
				void returnToInitialPosition();

				bool isDragging() { return this->dragging; };
				bool isEnabled() { return this->enabled; };
			private:
				sf::Vector2i initialPosition;

				bool dragging;
				bool enabled;

				void startDragging();
				void stopDragging();
				virtual void recalculatePosition();

				virtual sf::Vector2i getTotalSize();
		};

	};

};

#endif // DRAGGABLE_HPP_
