#ifndef IMAGE_HPP_
#define IMAGE_HPP_

#include "Gui/Element.hpp"

namespace LGE{

	namespace gui{

		class Image : public Element{
			public:
				Image(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				virtual void draw();
				virtual void handleInput(sf::Event *event);
				void setImageTexture(sf::Texture &texture);
				void setTextureRect(sf::IntRect rect);
			private:
				virtual void recalculatePosition();
				virtual sf::Vector2i getTotalSize();
		};

	};

};

#endif // IMAGE_HPP_