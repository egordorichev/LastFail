#ifndef INTERVAL_HPP_
#define INTERVAL_HPP_

#include <SFML/System.hpp>

#include <functional>

#include "Timer.hpp"

namespace LGE{

	class Interval{
		public:
			Interval(float time, std::function <void()> callback,
					bool paused = false) {

				this->paused = paused;
				this->timer = new Timer(time, callback, paused);
			};

			~Interval() {
				delete this->timer;
			}

			void stop() {
				this->timer->stop();
				this->paused = true;
			}

			void pause() {
				this->timer->pause();
				this->paused = true;
			}

			void resume() {
				this->timer->resume();
				this->paused = false;
			}
			void start() {
				this->timer->start();
				this->paused = false;
			}

			void setTime(float time) { this->timer->setTime(time); };
			void setCallback(std::function <void()> callback) { this->timer->setCallback(callback); };

			float getCurrentTime() { return this->timer->getCurrentTime(); };
			float getTime() { return this->timer->getTime(); };

			bool update(sf::Time &dt) {
				if(!this->paused) {
					if(this->timer->update(dt)) {
						this->timer->stop();
						this->timer->start();

						return true;
					}
				}

				return false;
			}

			bool isPaused() { return this->paused; };
		private:
			Timer *timer;

			bool paused;
	};

};

#endif // INTERVAL_HPP_
