#ifndef GAMESTATEMENU_HPP_
#define GAMESTATEMENU_HPP_

#include "Game/Game.hpp"
#include "Game/GameState.hpp"

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

namespace LGE{

	class GameStateMenu : public GameState{
		public:
			GameStateMenu(Game *game);

			void draw(sf::Time &dt);
			void update(sf::Time &dt);
			void handleInput();
		private:

	};

};

#endif // GAMESTATEMENU_HPP_
