#include "Gui/Button.hpp"
#include "Gui/Container.hpp"

#include "Log/InfoLog.hpp"

namespace LGE{

	namespace gui{

		const sf::IntRect buttonTextureRects[3][9] = {
			{
				sf::IntRect(0, 0, 8, 8), sf::IntRect(9, 0, 8, 8), sf::IntRect(18, 0, 8, 8),
				sf::IntRect(0, 9, 8, 8), sf::IntRect(9, 9, 8, 8), sf::IntRect(18, 9, 8, 8),
				sf::IntRect(0, 18, 8, 8), sf::IntRect(9, 18, 8, 8), sf::IntRect(18, 18, 8, 8)
			},
			{
				sf::IntRect(0, 27, 8, 8), sf::IntRect(9, 27, 8, 8), sf::IntRect(18, 27, 8, 8),
				sf::IntRect(0, 36, 8, 8), sf::IntRect(9, 36, 8, 8), sf::IntRect(18, 36, 8, 8),
				sf::IntRect(0, 45, 8, 8), sf::IntRect(9, 45, 8, 8), sf::IntRect(18, 45, 8, 8)
			},
			{
				sf::IntRect(0, 54, 8, 8), sf::IntRect(9, 54, 8, 8), sf::IntRect(18, 54, 8, 8),
				sf::IntRect(0, 63, 8, 8), sf::IntRect(9, 63, 8, 8), sf::IntRect(18, 63, 8, 8),
				sf::IntRect(0, 72, 8, 8), sf::IntRect(9, 72, 8, 8), sf::IntRect(18, 72, 8, 8)
			}
		};

		Button::Button(Container *super, sf::RenderWindow *window,
				sf::Texture &texture, sf::Font &font) : Element(super,
				window, texture, font) {

			this->label.setFont(this->font);
			this->label.setCharacterSize(24);
		}

		void Button::draw() {
			if(!this->hidden) {
				int variant;

				if(this->isHovered()) {
					variant = 1;
				} else if(this->isClicked()) {
					variant = 2;
				} else {
					variant = 0;
				}

				this->sprite.setTextureRect(buttonTextureRects[variant][0]); // Left top corner
				this->sprite.setPosition(this->rect.left, this->rect.top);

				window->draw(this->sprite);

				this->sprite.setTextureRect(buttonTextureRects[variant][1]); // Top side

				for(int i = 1; i < this->rect.width / 16; i++) {
					this->sprite.setPosition(this->rect.left + i * 16, this->rect.top);
					window->draw(this->sprite);
				}

				this->sprite.setTextureRect(buttonTextureRects[variant][2]); // Right top corner
				this->sprite.setPosition(this->rect.left + this->rect.width - 16, this->rect.top);

				window->draw(this->sprite);

				for(int i = 1; i < this->rect.height / 16; i++) {
					this->sprite.setTextureRect(buttonTextureRects[variant][3]); // Left side
					this->sprite.setPosition(this->rect.left, this->rect.top + i * 16);

					window->draw(this->sprite);

					this->sprite.setTextureRect(buttonTextureRects[variant][4]); // Center

					for(int j = 1; j < this->rect.width / 16; j++) {
						this->sprite.setPosition(this->rect.left + j * 16, this->rect.top + i * 16);
						window->draw(this->sprite);
					}

					this->sprite.setTextureRect(buttonTextureRects[variant][5]); // Right side
					this->sprite.setPosition(this->rect.left + this->rect.width - 16, this->rect.top + i * 16);

					window->draw(this->sprite);
				}

				this->sprite.setTextureRect(buttonTextureRects[variant][6]); // Left bottom corner
				this->sprite.setPosition(this->rect.left, this->rect.top + this->rect.height - 16);

				window->draw(this->sprite);

				this->sprite.setTextureRect(buttonTextureRects[variant][7]); // Bottom side

				for(int i = 1; i < this->rect.width / 16; i++) {
					this->sprite.setPosition(this->rect.left + i * 16, this->rect.top + this->rect.height - 16);
					window->draw(this->sprite);
				}

				this->sprite.setTextureRect(buttonTextureRects[variant][8]); // Right bottom corner
				this->sprite.setPosition(this->rect.left + this->rect.width - 16, this->rect.top + this->rect.height - 16);

				window->draw(this->sprite);
				window->draw(this->label);
			}
		}

		void Button::handleInput(sf::Event *event) {
			if(!this->hidden) {
				this->checkEvents(event);
			}
		}

		void Button::setLabel(std::string label) {
			this->label.setString(label);
			this->recalculatePosition();
		}

		void Button::recalculatePosition() {
			sf::FloatRect labelRect = this->label.getLocalBounds();

			this->label.setOrigin((int)(labelRect.left + labelRect.width / 2.0f),
				(int)(labelRect.top  + labelRect.height / 2.0f));

			this->label.setPosition(sf::Vector2f((int)(this->rect.width / 2.0f + this->rect.left),
				(int)(this->rect.height / 2.0f + this->rect.top + 3)));
		}

		sf::Vector2i Button::getTotalSize() {
			if(this->size == sf::Vector2i(0, 0)) {
				sf::FloatRect labelRect = this->label.getLocalBounds();

				return sf::Vector2i(labelRect.width + this->padding * 2,
					labelRect.height + this->padding * 2);
			} else {
				return sf::Vector2i(this->size.x + this->padding * 2,
					this->size.y + this->padding * 2);
			}
		}

	};

};