#include "World/World.hpp"
#include "World/Time.hpp"
#include "World/Chunk.hpp"

#include "Player/Player.hpp"
#include "Item/Item.hpp"

#include "Log/ErrorLog.hpp"
#include "Log/InfoLog.hpp"

#include "TextureManager.hpp"
#include "PerlinNoise.hpp"
#include "Crypt.hpp"

#include <SFML/Graphics.hpp>

#include <fstream>
#include <iostream>
#include <future>

#include <boost/filesystem.hpp>

namespace LGE{

	World::World(Game *game) {
		this->width = 32768;
		this->height = 4096;
		this->name = "";
		this->loaded = false;

		this->game = game;
		this->mobManager = new MobManager(this->game);
		this->mobManager->load("data/mobs/default.json");

		this->sky.setTexture(this->game->getTextureManager()->getTexture("sky", "assets/images/skybox.png"));
		this->sky.setScale(this->game->getWindow()->getSize().x * 1024 /
			this->sky.getLocalBounds().width, 1300 /
			this->sky.getLocalBounds().height); // http://stackoverflow.com/a/21541729/4741065
	}

	World::~World() {
		delete this->time;
		delete this->mobManager;

		for(auto mob : this->mobs) {
			delete mob;
		}

		this->mobs.clear();

		for(auto chunk : this->loadedChunks) {
			delete chunk;
		}

		this->loadedChunks.clear();
	}

	void World::spawnMob(int id, int x, int y) {
		if(this->loaded) {
			Mob *mob = this->mobManager->getMob(id);

			if(mob != nullptr) {
				mob->spawn(x, y);
				mob->setTarget(this->game->getPlayer());

				this->mobs.push_back(mob);
			}
		}
	}

	void World::load(std::string name) {
		if(!this->loaded) {
			this->directory = WORLDS_DIRECTORY + name + "/";

			if(boost::filesystem::exists(this->directory)) {
				infoLog << "Loading world...";

				this->preloadVisiableChunks();

				for(int i = this->game->getPlayer()->getLeft() / 16 - 4;
						i < this->game->getPlayer()->getLeft() / 16 + 4; i++) {

					for(int j = this->game->getPlayer()->getTop() / 16 - 4;
						j < this->game->getPlayer()->getTop() / 16 + 4; j++) {

							this->getItem(i, j)->destroy();
					}
				}

				this->time = new Time(7, 0, 0); // TODO: load from file
			} else {
				infoLog << "Generating new world...";

				this->generate(name);
			}

			infoLog << "Done";
			this->loaded = true;
		} else {
			errorLog << "Couldn't load world: it's already loaded";
		}
	}

	void World::save() {
		if(this->loaded) {
			infoLog << "Saving world...";

			boost::filesystem::create_directory(directory);
			boost::filesystem::create_directory(directory + "chunks/");

			for(auto chunk : this->loadedChunks) {
				chunk->save(this->directory + "chunks/");
			}

			infoLog << "Done";
		} else {
			errorLog << "Couldn't save world: it's not loaded";
		}
	}

	void World::generate(std::string name) {
		if(!this->loaded) {
			this->directory = WORLDS_DIRECTORY + name + "/";

			this->name = name;
			this->width = 32786; // In blocks!
			this->height = 4096;

			this->preloadVisiableChunks();

			this->loaded = true;

			this->save();
			this->time = new Time(7, 0, 0);
		}
	}

	void World::preloadChunkInThread(int x, int y) {
		x /= CHUNK_SIZE;
		y /= CHUNK_SIZE;

		Chunk *chunk = this->chunks[x][y];

		if(chunk != nullptr && chunk->isLoaded()) {
			return;
		}

		chunk = new Chunk(x, y);

		addThread([&]() {
			Chunk *chunk = this->chunks[x][y];
			chunk->load(this->directory + "chunk/", this->game->getItemManager());
			// this->loadedChunks.push_back(chunk);
			// this->updateChunkNeighbors(chunk);
		});

	}

	Chunk *World::preloadChunk(int x, int y) {
		x /= CHUNK_SIZE;
		y /= CHUNK_SIZE;

		Chunk *chunk = this->chunks[x][y];

		if(chunk != nullptr && chunk->isLoaded()) {
			return chunk;
		}

		if(chunk == nullptr) {
			chunk = new Chunk(x, y);
		}

		if(!chunk->isLoaded()) {
			chunk->load(this->directory + "chunk/", this->game->getItemManager());
		}

		// this->chunks[x][y] = chunk; // Horay, bug fixed!
		this->loadedChunks.push_back(chunk);
		// this->updateChunkNeighbors(chunk);

		return chunk;
	}

	Chunk *World::getChunk(int x, int y) {
		if(this->loaded) {
			int chunkX = x / CHUNK_SIZE;
			int chunkY = y / CHUNK_SIZE;

			Chunk *chunk = this->chunks[chunkX][chunkY];

			if(chunk == nullptr) {
				chunk = preloadChunk(x, y);
			}

			return chunk;
		}
	}

	void World::preloadVisiableChunks() {
		int windowWidth = this->game->getWindow()->getSize().x;
		int windowHeight = this->game->getWindow()->getSize().y;
		int playerLeft = this->game->getPlayer()->getLeft();
		int playerTop = this->game->getPlayer()->getTop();

		for(int i = (this->game->getPlayer()->getLeft() / 16 - windowWidth / 32) / CHUNK_SIZE ;
				i < (this->game->getPlayer()->getLeft() / 16 + windowWidth / 32) / CHUNK_SIZE + 1; i++) {

			for(int j = (this->game->getPlayer()->getTop() / 16 - windowHeight / 32) / CHUNK_SIZE ;
					j < (this->game->getPlayer()->getTop() / 16 + windowHeight / 32) / CHUNK_SIZE + 1; j++) {

				Chunk *chunk = this->chunks[i][j];

				if(chunk == nullptr) {
					this->chunks[i][j] = this->preloadChunk(i * CHUNK_SIZE, j * CHUNK_SIZE);
				}

				this->updateChunkNeighbors(this->chunks[i][j]);
			}
		}
	}

	void World::updateChunkNeighbors(Chunk *chunk) {
		if(chunk != nullptr) {
			int x = chunk->getX();
			int y = chunk->getY();

			Chunk *chunkNeighbors[8] = {
				this->chunks[x - 1][y - 1], this->chunks[x][y - 1],
				this->chunks[x + 1][y - 1], this->chunks[x - 1][y],
				this->chunks[x + 1][y], this->chunks[x - 1][y + 1],
				this->chunks[x][y + 1], this->chunks[x + 1][y + 1]
			};

			chunk->updateSpriteRects();

			for(auto chunk : chunkNeighbors) {
				if(chunk != nullptr) {
					chunk->updateSpriteRects();
				}
			}
		}
	}

	void World::draw(sf::RenderWindow *window, sf::Time &dt) {
		if(this->loaded) {
			int windowWidth = this->game->getWindow()->getSize().x;
			int windowHeight = this->game->getWindow()->getSize().y;
			int playerLeft = this->game->getPlayer()->getLeft();
			int playerTop = this->game->getPlayer()->getTop();

			int count = 0;

			for(int i = (this->game->getPlayer()->getLeft() / 16 - windowWidth / 32) / CHUNK_SIZE - 1;
					i < (this->game->getPlayer()->getLeft() / 16 + windowWidth / 32) / CHUNK_SIZE + 1; i++) {

				for(int j = (this->game->getPlayer()->getTop() / 16 - windowHeight / 32) / CHUNK_SIZE - 1;
						j < (this->game->getPlayer()->getTop() / 16 + windowHeight / 32) / CHUNK_SIZE + 1; j++) {

					Chunk *chunk = this->chunks[i][j];

					if(chunk != nullptr) {
						chunk->draw(window);
					}
				}
			}

			/*for(Mob *mob : this->mobs) {
				mob->draw(window);
			}*/
		}
	}

	void World::drawBackgrounds(sf::RenderWindow *window) {
		this->sky.setTextureRect(sf::IntRect(this->time->getTotalTime() / 215 + 119 /* Changable */, 0, 1, 1024));
		window->draw(this->sky);
	}

	void World::handleInput(sf::Event *event) {
		if(event->type == sf::Event::Resized) {
			sky.setScale(event->size.width * 1024 /
				sky.getLocalBounds().width, 1300 /
				sky.getLocalBounds().height);
		}
	}

	void World::update(sf::Time &dt) {
		/*this->time->update(dt);

		for(Mob *mob : this->mobs) {
			std::vector <Item *> items;

			for(int i = mob->getLeft() / 16 - mob->getWidth() / 32 - 2; i < mob->getLeft() / 16 + mob->getWidth() / 32 + 2; i++) {
				for(int j = mob->getTop() / 16 - mob->getHegiht() / 32 - 2; j <  mob->getTop() / 16 + mob->getHegiht() + 2; j++) {
					items.push_back(this->getItem(i, j));
				}
			}

			mob->update(items, dt);
		}*/
	}

	void World::placeItem(Item *item, int x, int y) {
		if(this->loaded) {
			this->getChunk(x, y)->placeItem(item, x, y);
		}
	}

	Item *World::getItem(int x, int y) {
		int chunkX = x / CHUNK_SIZE;
		int chunkY = y / CHUNK_SIZE;

		Chunk *chunk = this->chunks[chunkX][chunkY];

		if(chunk == nullptr) {
			return this->preloadChunk(chunkX, chunkY)->getItem(x, y);
		} else {
			return chunk->getItem(x, y);
		}
	}

};