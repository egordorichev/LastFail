#ifndef ANIMATION_HPP_
#define ANIMATION_HPP_

#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "Animation/AnimationFrame.hpp"

#include <vector>

namespace LGE{


	class Animation{
		public:
			Animation(sf::Sprite *sprite, bool paused = false, bool looped = true);

			void update(sf::Time dt);
			void addFrame(AnimationFrame frame);
			void addFrame(sf::IntRect rect, sf::Time frameTime);
			void setCurrentFrame(int frame);
			void play();
			void pause();
			void stop();
			void setLooped(bool looped) { this->looped = looped; };
			void setSprite(sf::Sprite *sprite) { this->sprite = sprite; };

			bool isLooped() { return this->looped; };
			bool isPaused() { return this->paused; };
		private:
			sf::Sprite *sprite;
			sf::Time currentTime;

			std::vector <AnimationFrame> frames;

			float frame;

			bool paused;
			bool looped;
	};

};

#endif // ANIMATION_HPP_