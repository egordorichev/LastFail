#include "Game/Game.hpp"
#include "Game/GameState.hpp"
#include "Game/GameStateGP.hpp"
#include "Game/GameStateMenu.hpp"

int main() {
	LGE::Game Game("Last Fail 0.1 develop");

	Game.init();
	Game.pushState(new LGE::GameStateGP(&Game));
	// Game.pushState(new LGE::GameStateMenu(&Game));
	Game.run();

	return 0;
}