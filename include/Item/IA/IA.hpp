#ifndef IA_HPP_
#define IA_HPP_

#include "Item/Item.hpp"

namespace LGE{

	class Game;

	class IA{
		public:
			IA(Game *game) { this->game = game; };

			virtual void onPlace(Item *item) { };
			virtual void onDestroy(Item *item) { };
			virtual void onRightClick(Item *item) { };
			virtual void onLeftClick(Item *item) { };
			virtual void onRectChange(Item *item) { };
			virtual void proccess(Item *item) { };

			virtual sf::IntRect getTextureRect() { return sf::IntRect(0, 0, 8, 8); };
			virtual std::string getName() { return "IA"; };

			virtual int getId() { return 0; };
		protected:
			Game *game;
	};

};

#endif // IA_HPP_