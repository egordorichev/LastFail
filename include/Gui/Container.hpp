#ifndef CONTAINER_HPP_
#define CONTAINER_HPP_

#include "Log/ErrorLog.hpp"
#include "Gui/Element.hpp"

#include <string>
#include <unordered_map>

namespace LGE{

	namespace gui{

		class Draggable;

		class Container : public Element{
			public:
				Container(Container *super, sf::RenderWindow *window,
					sf::Texture &texture, sf::Font &font);

				~Container() { this->removeAll(); };

				void draw();
				void handleInput(sf::Event *event);
				bool handleDragEnd(Draggable *draggable);
				void recalculatePositions();

				template <class T> T *add(std::string name) {
					if(this->elements.find(name) != this->elements.end()) {
						errorLog << "Element with name \"" + name + "\" already exists in container";
					} else {
						T *element = new T(this, this->window,
							this->texture, this->font);

						element->onRectChange();

						this->elements[name] = element;

						return element;
					}
				}

				template <class T> T *add() {
					return this->add <T> (std::to_string(this->nextId++));
				}

				template <class T> T *get(std::string name) {
					if(this->elements.find(name) == this->elements.end()) {
						errorLog << "Element with name \"" + name + "\" is not found in container";
					} else {
						return static_cast <T *> (this->elements[name]);
					}
				}

				void remove(std::string name);
				void removeAll();
				void show();
				void hide();
			private:
				std::unordered_map <std::string, Element *> elements;

				int nextId;

				void recalculatePosition();
				sf::Vector2i getTotalSize();
		};

	};

};

#endif // CONTAINER_HPP_