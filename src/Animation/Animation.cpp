#include "Animation/Animation.hpp"
#include <SFML/Graphics.hpp>

#include <vector>
#include <iostream>

namespace LGE{

	Animation::Animation(sf::Sprite *sprite, bool paused, bool looped) {
		this->paused = paused;
		this->looped = looped;
		this->sprite = sprite;
		this->currentTime = sf::Time::Zero;
		this->frame = 0;
	}

	void Animation::addFrame(AnimationFrame frame) {
		this->frames.push_back(frame);

		if(this->sprite != nullptr) {
			this->sprite->setTextureRect(this->frames[this->frame].getRect());
		}
	}

	void Animation::addFrame(sf::IntRect rect, sf::Time frameTime) {
		this->addFrame(AnimationFrame(rect, frameTime));
	}

	void Animation::setCurrentFrame(int frame) {
		this->frame = frame;

		if(this->sprite != nullptr) {
			this->sprite->setTextureRect(this->frames[this->frame].getRect());
		}
	}

	void Animation::update(sf::Time dt) {
		if(!this->paused && !this->frames.empty()) {
			this->currentTime += dt;

			if(this->currentTime >= this->frames[this->frame].getFrameTime()) {
				this->currentTime = sf::Time::Zero;

				if(this->frame + 1 < this->frames.size()) {
					this->frame++;
				} else {
					this->frame = 0;

					if(!this->looped) {
						this->paused = true;
					}
				}

				if(this->sprite != nullptr) {
					this->sprite->setTextureRect(this->frames[this->frame].getRect());
				}
			}
		}
	}

	void Animation::play() {
		this->paused = false;
	}

	void Animation::pause() {
		this->paused = true;
	}

	void Animation::stop() {
		this->paused = true;
		this->frame = 0;
	}

};